Objectives :

* Describe what data munging is.
* Demonstrate how to read a CSV data file .
* Explain how to select,remove,rename rows abd columns.
* Assess why data scientists need to be able to munge data


DATA MUNGING is the process of turning data set with a bunch of junk in it into a nice clean data set.

How does one transform a massive, inconsistent spreadsheet of transactions riddled with typos and bad delimiters into structured and trusted input for use in sophisticated analytics ? Worse yet - what if it’s not even a spreadsheet, but a raw webpage, a thousand emails, a text file containing a billion error logs, or a collection of unstructured documents stored haphazardly in a cloud? 
                                   ***** Then Data Munging comes into the picture *****
 ---Data munging is a set of concepts and a methodology for taking data from unusable and erroneous forms to the new levels of structure and quality required by modern analytics processes and consumers.---

The most basic munging operations can be performed in generic tools like Excel or Tableau —from searching for typos to using pivot tables, or the occasional informational visualization and simple macro.

In python , with one of the largest collections of third-party libraries, especially rich data processing and analysis tools like Pandas, NumPy, and SciPy, Python simplifies many complex data munging tasks. Pandas in particular is one of the fastest-growing and best-supported data munging libraries, while still only a tiny part of the massive Python ecosystem. 

CSV :
    Comma Separated Values . How to read in a data set that is stored as a comma delimited text file (CSV File) that needs to be cleaned up ..?
	A CSV file (Comma Separated Values file) is a type of plain text file that uses specific structuring to arrange tabular data. Because it’s a plain text file, it can contain only actual text data—in other words, printable ASCII or Unicode characters.

The structure of a CSV file is given away by its name. Normally, CSV files use a comma to separate each specific data value. Here’s what that structure looks like:

		column 1 name,column 2 name, column 3 name
		first row data 1,first row data 2,first row data 3
		second row data 1,second row data 2,second row data 3
		...
CSV files are normally created by programs that handle large amounts of data. They are a convenient way to export data from spreadsheets and databases as well as import or use it in other programs

Parsing CSV Files With Python’s Built-in CSV Library
The csv library provides functionality to both read from and write to CSV files. Designed to work out of the box with Excel-generated CSV files, it is easily adapted to work with a variety of CSV formats. The csv library contains objects and other code to read, write, and process data from and to CSV files.

Reading CSV Files With csv
Reading from a CSV file is done using the reader object. The CSV file is opened as a text file with Python’s built-in open() function, which returns a file object. This is then passed to the reader, which does the heavy lifting.

Here’s the employee_birthday.txt file:

name,department,birthday month
John Smith,Accounting,November
Erica Meyers,IT,March

code :
the code to read it in as a dictionary:

import csv

with open('employee_birthday.txt', mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        print(f'\t{row["name"]} works in the {row["department"]} department, and was born in {row["birthday month"]}.')
        line_count += 1
    print(f'Processed {line_count} lines.')

This results in the same output as before:
in Shell...:

Column names are name, department, birthday month
    John Smith works in the Accounting department, and was born in November.
    Erica Meyers works in the IT department, and was born in March.
Processed 3 lines.

