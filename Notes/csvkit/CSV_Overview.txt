csvkit is a library optimized for working with CSV files. It’s written in Python but the primary interface is the command line.All csvkit utilities accept an input file as “standard in”, in addition to as a filename. This means that we can make the output of one csvkit utility become the input of the next.

This means we can pipe the stdout of csvcut to the stdin of csvlook

You can install csvkit using pip:
                                 pip install csvkit

Why csvkit?
1.Because it makes your life easier.
2.If you have Excel you can open the file and take a look at it, but really, who wants to wait for Excel to load? Instead, let’s convert it to 
  a CSV
3.all csvkit tools accept an input file via “standard in”. This means that, using the | (“pipe”) character we can use the output of one 
  csvkit tool as the input of the next.Piping is a core feature of csvkit. Of course, you can always write the output of each command to a 
  file using >. However, it’s often faster and more convenient to use pipes to chain several commands together.


csvkit is composed of command-line tools that can be divided into three major categories: Input, Processing, and Output.

             Input
in2csv
sql2csv

             Processing
csvclean
csvcut
csvgrep
csvjoin
csvsort
csvstack
 
              Output and Analysis
csvformat
csvjson
csvlook
csvpy
csvsql
csvstat


                                                         



                                                   


                                                     
