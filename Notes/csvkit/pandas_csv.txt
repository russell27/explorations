
Using pandas with csv.................. 

#import numpy as np
import pandas as pd

s = pd.Series([12, -4, 7, 9])
print(s)

..................

import numpy as np
import pandas as pd

s = pd.Series([12, -4, 7, 9], index=['a', 'b', 'c', 'd'])
print(s)
print()

# Selecting elements
print(s[2])
print(s['b'])
print(s[0:2])
print(s[['b','c']])
print()

# Assignment
s[1] = 0
print(s)
print()

s['b'] = 1
print(s)
print()

# Filtering values
print(s[s>8])
print()

#Operations and math functions
print(s/2)
print()

print(np.log(s))
print()

...................................

import numpy as np
import pandas as pd

serd = pd.Series([1,0,2,1,2,3], index=['white','white','blue','green','green','yellow'])
print(serd)
print()

# All unique values without duplicates
print(serd.unique())
print()

# Return unique values with counting of occurences
print(serd.value_counts())
print()

# Evaluates values membership
print(serd.isin([0,3]))
print()
print(serd[serd.isin([0,3])])

............................................

import numpy as np
import pandas as pd

mydict1 = {'red':2000, 'blue': 1000, 'yellow':500, 'orange':1000}
myseries1 = pd.Series(mydict1)
print(myseries1)
print()

mydict2 = {'red':400, 'yellow':1000, 'black':700}
myseries2 = pd.Series(mydict2)
print(myseries2)
print()

myseries3 = myseries1 + myseries2
print(myseries3)

.....................................................

import numpy as np
import pandas as pd

data = {'color': ['blue','green','yellow','red','white'],
        'object': ['ball','pen','pencil','paper','mug'],
        'price':[1.2,1.0,0.6,0.9,1.7]}

print(data)
print()

frame = pd.DataFrame(data)
print(frame)

print(frame.ix[1])

.............................................................

import numpy as np
import pandas as pd

data = {'color': ['blue','green','yellow','red','white'],
        'object': ['ball','pen','pencil','paper','mug'],
        'price':[1.2,1.0,0.6,0.9,1.7]}

frame = pd.DataFrame(data)

print(frame.columns)
print()
print(frame.index)
print()
print(frame.values)
print()

print(frame['price'])
print()
print(frame.price)
print()
print(frame.ix[0])
print()
print(frame.ix[0]['price'])
print()
print(frame.ix[0].price)
print()

..........................................................................

import numpy as np
import pandas as pd

data = {'color': ['blue','green','yellow','red','white'],
        'object': ['ball','pen','pencil','paper','mug'],
        'price':[1.2,1.0,0.6,0.9,1.7]}

frame = pd.DataFrame(data)

print(frame.price)

for i in range(0, len(frame.index)):
    frame.loc[i,'price'] = 8.88

print(frame.price)
print()

frame.loc[2,'price'] = 9.99
print(frame.price)

............................................................................

import numpy as np
import pandas as pd

data1 = {'ball': [0,4,8,12],
        'pen': [1,5,9,13],
        'pencil': [2,6,10,14],
        'paper':[3,7,11,15]}

frame1 = pd.DataFrame(data1, index=['red','blue','yellow','white'])

print(frame1)
print()

data2 = {'mug': [0,3,6,9],
        'pen': [1,4,7,10],
        'ball': [2,5,8,11]}

frame2 = pd.DataFrame(data2, index=['blue','green','white','yellow'])

print(frame2)
print()

frame3 = frame1.add(frame2)
print(frame3)

In [ ]:

import numpy as np
import pandas as pd

data1 = {'ball': [0,4,8,12],
        'pen': [1,5,9,13],
        'pencil': [2,6,10,14],
        'paper':[3,7,11,15]}

frame1 = pd.DataFrame(data1, index=['red','blue','yellow','white'])

print(frame1.sum())
print()
print(frame1.mean())
print()
print(frame1.describe())

In [ ]:

import numpy as np
import pandas as pd

csvframe = pd.read_csv('D:/iPrimed/Persistence/Data/myCSV_01.csv') #Give correct path of the file
print(csvframe)

...................................................

import numpy as np
import pandas as pd

csvframe = pd.read_csv('D:/iPrimed/Persistence/Data/myCSV_01.csv')
print(csvframe)

csvframe['total'] = csvframe['white'] + csvframe['red'] + csvframe['blue'] + csvframe['green']
csvframe['excess'] = False

for i in range(len(csvframe)):
        if csvframe.loc[i,'total'] > 15:
                csvframe.loc[i,'excess'] = True

print(csvframe)

csvframe = csvframe.drop('total', axis=1)
csvframe = csvframe.drop(0, axis=0)


print(csvframe)
