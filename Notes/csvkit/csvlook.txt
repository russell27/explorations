                                                          *** complete description of csvlook****

--> WHAT IT DOES? -

       --Render a CSV file in the console as a Markdown-compatible, fixed-width table
            
       -- If file comtains                  
                 names
                nikhita
                nithish
                sridevi
                narasimha raju
          now if we use csvlook on that file it gives output as standard out -

              | names          |
              | -------------- |
	          | nikhita        |
	          | nithish        |
	          | sridevi        |
	          | narasimha raju |
	          |                |
	  -- If a table is too wide to display properly try piping the output to 'less -S' or truncating it using csvcut.


--> IT HAS "47" LINES OF CODE IN PYTHON

--> COMPLETE DESCRIPTION OF CODE:
        
      1> It imported 2 libraries.
            1) agate

                    -- agate is a data analysis library in python for humans instead of machines.It's an alternative for numpy and pandas

                    -- With agate we can load data from CSV to print it in taable format using Table.from.csv class
             
                    -- Agate helps in Describing the table, Navigating table data, Aggregating column data, Selecting and filtering, Computing new columns, sorting and slicing, grouping and 
		       aggregating, Multi dimentional aggregation, Exploring chating.

                    -- For agate in setup.py they installed.
                            'agate>=1.6.1',
    			    'agate-excel>=0.2.2',
    			    'agate-dbf>=0.2.0',
    			    'agate-sql>=0.5.3'


            2) CSVKitUtility from csvkit.cli

                    -- CSVKitUtility is a class written in cli.py file in csvkit

                    -- class CSVLook inherits from CSVKitUtility
          
                    -- CONTENTS IN THIS CLASS:
                             
                              1> Three instance variables description = '', epilog = '', override_flags = ''... we can use these in CSVLook like we used description.

                              2> It has 15 functions 
                                     
                                    1> __init__(self, args=None, output_file=None)

                                    2> add_arguments(self)

                                    3> run(self)
                                 
                                    4> main(self)

                                    5> _init_common_parser(self)

                                    6>  _open_input_file(self, path).
                 
                                    7> _extract_csv_reader_kwargs(self)

                                    8> _extract_csv_writer_kwargs(self)
               
                                    9>  _install_exception_handler(self)
                             
                                    10> get_column_types(self)

                                    11> get_column_offset(self)

                                    12> skip_lines(self)

                                    13> get_rows_and_column_names_and_column_ids(self, **kwargs)

                                    14> print_column_names(self)
                                     
                                    15> additional_input_expected(self)

                              3> EXPLAINATION FOR EACH FUNTION:

                                    1>init function -  It has arguments self, args=None and output_file=None. This function Perform argument processing and other setup for a CSVKitUtility.

                                    2> add_arguments() - Called upon initialization once the parser for common arguments has been constructed. Should be overriden by individual utilities,
                                                         simply these take the strings on the command line and turn them into objects

          	   		    
				                    3> run(self) - A wrapper around the main loop of the utility which handles opening and closing files

 				                    4> main(self) -  Main loop of the utility. Should be overriden by individual utilities and explicitly called by the executing script.

 				                   5> self_init_commom_parser() - Prepare a base argparse argument parser so that flags are consistent across different shell command tools. If you want to 
 				                                   constrain which common args are present, you can pass a string for 'omitflags'. Any argument whose single-letter form is 
 				                                   contained in 'omitflags' will be left out of the configured parser. Use 'f' for file. in a single line it takes all the 
 				                                   possible arguments and process with it.

                     	            6> _open_input_file(self, path) - Open the input file specified on the command line.

                                    7> _extract_csv_reader_kwargs(self) - Extracts those from the command-line arguments those would should be passed through to the input CSV 
                                                                          reader(s).
     
                                    8> _extract_csv_writer_kwargs(self) -  Extracts those from the command-line arguments those would should be passed through to the output CSV 
                                                                           writer.
    
                                    9>_install_exception_handler(self) - Installs a replacement for sys.excepthook, which handles pretty-printing uncaught exceptions.
     
                                    10> get_column_types(self) - It just gets the column type. sees the order in agate type tester.

                                    11> get_column_offset(self) - Still figuring 

                                    12> skip_lines(self) - we will give args.skip_lines as integer input and then read input file and decrement the args.skip_lines variable. 
                                                           It returns input_file

                                    13> get_rows_and_column_names_and_column_ids(self, **kwargs) - As name indicates it gets the row and cloumn names and colum ids

                                    14> print_column_names(self) - Pretty-prints the names and indices of all columns to a file-like object (usually sys.stdout) 
                                                                (pretty-prints prints it in a table format)
                                     
                                    15> additional_input_expected(self) -  return sys.stdin.isatty() and not self.args.input_path.

       


       

      3> Created a class CSVLook with inherits from CSVKitUtility
  
            -- description = 'Render a CSV file in the console as a Markdown-compatible, fixed-width table.' description is from CSVKitUtility.
        
            -- This class has two functions.
          
                  1) add_arguments
                 
                       -- This function is to check all the optional arguments.
                    
                       -- optional arguments:
                                 
                                 -h, --help                                      show this help message and exit
                                 
                                 --max-rows MAX_ROWS                             The maximum number of rows to display before truncating the data.
                                 
                                 --max-columns MAX_COLUMNS                       The maximum number of columns to display before truncating the data.
                                  
                                 --max-column-width MAX_COLUMN_WIDTH             Truncate all columns to at most this width. The remainder will be replaced with ellipsis.
                                
                                 -y SNIFF_LIMIT, --snifflimit SNIFF_LIMIT        Limit CSV dialect sniffing to the specified number of bytes. Specify "0" to disable sniffing 
                                                                                 entirely.
  
                                 -I, --no-inference                              Disable type inference when parsing the input.


                          -- argparser.add_argument() - The argparse module makes it easy to write user-friendly command-line interfaces. this calls tell the ArgumentParser 
                                                        how to take the strings on the command line and turn them into objects. This information is stored and used when 
                                                        parse_args() is called.

                           -- argparse version used 1.2.1



                  2) main():
                           --- it hs a if condition which checks the function in CSVKitUtility class that is additional_input_exceed() if it's true then it gives a msg that 
                               input should be properly typed

                           --- else 

                              * This is to take input from a file

                                  A variable table stored the return value or object of this function agate.Table.from_csv(with arguments)
                      
                                   -- agate.Table.from_csv() this function loads data from CSV file
                 
                                   -- arguments passed in this function are
  
                                         1> self.input_file  - this is input_file
 
                                         2> skip_lines=self.args.skip_lines  - self.args.skip_lines is a variable that contains integer of that class that indicates no.of.lines
                                                                               to skip  and it is assigned to skip_lines

                                         3> sniff_limit=self.args.sniff_limit - self.args.sniff_limit is a variable that contains integer of that class that indicates sniff 
                                                                                limit. and it is assigned to sniff_limit

                                         4> column_types=self.get_column_types() - get_column_types() is a function in CSVKitUtillity class that returns the type of column and 
                                                                                   assigned to column_types

                                         5> line_numbers=self.args.line_numbers - self.args.line_numbers is a variable that contains integer contains line number. and it is 
                                                                                  assigned to line_numbers
                                         6> **self.reader_kwargs  - keyword arguments to pass to specific reader instances to further configure file searching. or another 
                                                                    keyword arguments to pass to reader creation

                                * This is to print output on terminal

                                      Table.print_table() this function is to print the data in table form as output.
                       
                                          -- arguments passed in this function are

                                                 1> output=self.output_file - this is the output_file given to output variable

                                                 2> max_rows=self.args.max_rows - this contains maximum rows

                                                 3> max_columns=self.args.max_columns - this contains maximum columns

                                                 4> max_column_width=self.args.max_column_width - this contains maximum column width if any column element exceeds 
                                                                                                  that limit it won't print.
                                   
   
    4> After CSVlook class launch_new_instance() this function is created which creates object for CSVLook and runs it with run() function already in CSVKitUtility class

    5> finally calling launch_new_instance() im main() fuction.



                                                                                --- The End ---

