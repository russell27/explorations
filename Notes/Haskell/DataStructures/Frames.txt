Performing data analysis in Haskell brings with it a few advantages:

* Interactive exploration is supported in GHCi
* GHC produces fast, memory-efficient code when you're ready to run a program that might take a bit of time.
* You get to use Haskell to write your own functions when what you want isn't already defined in the library.
* The code you write is statically typed so that mismatches between your code and your data data are found by the type checker.

A Frame is an in-memory representation of your data. The Frames library stores each column as compactly as it knows how,
and lets you index your data as a structure of arrays, or as an array of structures, also known as a Frame.

A Frame provides O(1) indexing, as well as any other operations you are familiar with based on the Foldable class. If a data set is small,
keeping it in RAM is usually the fastest way to perform multiple analyses on that data that you can't fuse into a single traversal.

User-friendly, type safe, runtime efficient tooling for working with tabular data deserialized from comma-separated values (CSV) files.

In the common case where a data file has a header row providing column names, and columns are separated by commas, generating the types 
needed to import a data set is as simple as,
tableTypes "User" "data/ml-100k/u.user"

The data set this example considers is rather far from the sweet spot of CSV processing that Frames is aimed it: it does not include column headers,
nor does it use commas to separate values! However, these mismatches do provide an opportunity to see that the Frames library is flexible enough to 
meet a variety of needs.
tableTypes' (rowGen "data/ml-100k/u.user")
            { rowTypeName = "User"
            , columnNames = [ "user id", "age", "gender"
                            , "occupation", "zip code" ]
            , separator = "|" }
            
Example:
λ> ms <- loadMovies
λ> L.fold L.minimum (view age <$> ms)
Just 7

When there are multiple properties we would like to compute, we can fuse multiple traversals into one pass using something like the foldl package.
minMax :: Ord a => L.Fold a (Maybe a, Maybe a)
minMax = (,) <$> L.minimum <*> L.maximum

λ> L.fold (L.handles age minMax) ms
(Just 7,Just 73)
Here we are projecting the age column out of each record, and computing the minimum and maximum age across all rows.

