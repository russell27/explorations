﻿what is SubSetting ?
 * It is the process of retrieving just the parts of large files which are of interest for a specific purpose .
--It includes-
    * Row Subset
    * Column Subset
    * Query / Conditional Subset
    * Column Subset Update
Here we continue with same previous example in frames.txt.
import qualified Data.Foldable as F

1.Rows Subset
(import qualified Data.Foldable as F)
*To get first three rows 

    λ> mapM_ print (take 3 (F.toList ms))
    {user id :-> 1, age :-> 24, gender :-> "M", occupation :-> "technician", zip code :-> "85711"}
    {user id :-> 2, age :-> 53, gender :-> "F", occupation :-> "other", zip code :-> "94043"}
    {user id :-> 3, age :-> 23, gender :-> "M", occupation :-> "writer", zip code :-> "32067"}

*To get last three rows

    λ> mapM_ (print . frameRow ms) [frameLength ms - 3 .. frameLength ms - 1]
    {user id :-> 941, age :-> 20, gender :-> "M", occupation :-> "student", zip code :-> "97229"}
    {user id :-> 942, age :-> 48, gender :-> "F", occupation :-> "librarian", zip code :-> "78209"}
    {user id :-> 943, age :-> 22, gender :-> "M", occupation :-> "student", zip code :-> "77841"}

*To get subset of rows

    λ> mapM_ (print . frameRow ms) [50..55]
    {user id :-> 51, age :-> 28, gender :-> "M", occupation :-> "educator", zip code :-> "16509"}
    {user id :-> 52, age :-> 18, gender :-> "F", occupation :-> "student", zip code :-> "55105"}
    {user id :-> 53, age :-> 26, gender :-> "M", occupation :-> "programmer", zip code :-> "55414"}
    {user id :-> 54, age :-> 22, gender :-> "M", occupation :-> "executive", zip code :-> "66315"}
    {user id :-> 55, age :-> 37, gender :-> "M", occupation :-> "programmer", zip code :-> "01331"}
    {user id :-> 56, age :-> 25, gender :-> "M", occupation :-> "librarian", zip code :-> "46260"}
2.Column Subset
*consider a single column
    λ> take 6 . F.toList $ view occupation <$> ms
    ["technician","other","writer","technician","other","executive"]
*consider multiple columns
    miniUser :: User -> Record '[Occupation, Gender, Age]
    miniUser = rcast 

    λ> mapM_ print . take 4 . F.toList $ fmap miniUser ms
    {occupation :-> "technician", gender :-> "M", age :-> 24}
    {occupation :-> "other", gender :-> "F", age :-> 53}
    {occupation :-> "writer", gender :-> "M", age :-> 23}
    {occupation :-> "technician", gender :-> "M", age :-> 24}
    
Instead of using function like miniuser,fix the types in-line by using the rcast function. 
    λ> :set -XTypeApplications -XDataKinds
    λ> rcast @'[Occupation,Gender,Age] $ frameRow ms 0
    {occupation :-> "technician", gender :-> "M", age :-> 24}

3.Query / Conditional Subset
*when specific need to be fulfilled.This can be done in two ways:
Using Pipes
(import qualified Pipes.Prelude as P)

    writers :: (Occupation ∈ rs, Monad m) => Pipe (Record rs) (Record rs) m r
    writers = P.filter ((== "writer") . view occupation)

    λ> runSafeEffect $ movieStream >-> writers >-> P.take 4 >-> P.print
    {user id :-> 3, age :-> 23, gender :-> "M", occupation :-> "writer", zip code :-> "32067"}
    {user id :-> 21, age :-> 26, gender :-> "M", occupation :-> "writer", zip code :-> "30068"}
    {user id :-> 22, age :-> 25, gender :-> "M", occupation :-> "writer", zip code :-> "40206"}
    {user id :-> 28, age :-> 32, gender :-> "M", occupation :-> "writer", zip code :-> "55369"}

or helper function by frames
    λ> pipePreview movieStream 4 writers

*Handy to try out various maps and filters and apply to large data

4.Column Subset update
*A Function can be applied to columns of each row without disturbing the rest.
    intFieldDoubler :: Record '[UserId, Age] -> Record '[UserId, Age]
    intFieldDoubler = mapMono (* 2)

    λ> pipePreview movieStream 3 (P.map (rsubset %~ intFieldDoubler))
    {user id :-> 2, age :-> 48, gender :-> "M", occupation :-> "technician", zip code :-> "85711"}
    {user id :-> 4, age :-> 106, gender :-> "F", occupation :-> "other", zip code :-> "94043"}
    {user id :-> 6, age :-> 46, gender :-> "M", occupation :-> "writer", zip code :-> "32067"}

*As mentioned in the function doubling of user id and age column is done.


