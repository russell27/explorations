--> What are crates and where can you find them ?

    Crates are a similar to the packages in some other languages.
    
    All pre - existing crates are published on crates.io . If you've got a library that you'd like to share with the world, you can publish it on crates.io too.
    
    There are 38,459 crates available on crates.io as of now. 
     
    
     
--> How to include an external crate ?
    
    To depend on a library hosted on crates.io, add it to your Cargo.toml.
    
    List the crate name, version you would like to use in the dependencies section. For example if you want to include the time crate wih version say "0.1.12", then,
    
    list it in the dependency section like this :
    
    [dependencies]
    time = "0.1.12"
    
    
--> Some of the most commonly used crates are :

    --> rand :
    
        A Rust library for random number generation.
        
        
        Required changes in Cargo.toml :
        
        Add rand = "0.7.3" under dependencies section.
        
        
        
        For example :
        If we write the below code in our file :
        
        let x = rand::random::<u8>();
        println!("{}", x);
        
        Everytime we run the file an unsigned integer which occupies <= 8 bites is printed randomly.
        