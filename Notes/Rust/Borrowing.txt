                                                             RUST — Borrowing

- It is very inconvenient to pass the ownership of a variable to another function and then return the ownership, So Rust supports a concept, borrowing, where the ownership of a value is transferred temporarily to an entity and then returned to the original owner entity.

fn main(){

  let v = vec![10,20,30];
  print_vector(v);
  println!("{}",v[0]);
}

fn print_vector(x:Vec<i32>){
  println!("Inside print_vector function {:?}",x);
}

The above code will result in an error as the main() function is trying to access the vector v whose ownership is passed to the print_vector() function from the main().

                                                             What is Borrowing?

- When a function transfers its control over a variable to another function temporarily, for a while then it is called borrowing.

- This is achieved by passing a reference to the variable rather than passing the value itself to the function.

- In simple words, Borrowing means ownership of the variable/ value is transferred to the original owner of the variable after the function to which the control was passed completes execution.

fn main (){
   let v = vec![10, 20, 30] ;
   print_vector(&v);
   println!("Printing the value from main() v[0]={}",v[0]);
 }
fn print_vector(x:&Vec<i32>){
   println!("Inside print_vector function {:?}",x);
}

Output :

Inside print_vector function [10, 20, 30]
Printing the value from main() v[0]=10

                                                           Mutable References

- A function can modify a borrowed resource by using a mutable reference to such resource.

- This reference is prefixed with &mut.

- They can operate only on mutable variables.

- example for Mutating an integer reference

fn add_one(e: &mut i32) {
   *e+= 1;
}
fn main() {
   let mut i = 3;
   add_one(&mut i);
   println!("{}", i); 
}

output:
4

- example for Mutating a string reference

fn main(){
   let mut name : String = String :: from("Rust");
   display( &mut name ) ;
   println!("The value of name after modification is : {}",name);
}
fn display(param_name:&mut String){
   println!("param_name value is :{}",param_name);
   param_name.push_str(" Language");
}

Output:

param_name value is :Rust
The value of name after modification is:Rust Language
