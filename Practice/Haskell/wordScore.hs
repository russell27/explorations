import Data.Char (toLower)
import Data.List

avg a b = (a + b) `div` 2
countGroup s g = length [ch | ch <- s, elem (toLower ch) g]
countVowels = countGroup "aeiou"
countConsos = countGroup "bcdfghjklmnpqrstvwxyz"
score s = (avg (countVowels s) (countConsos s))

main = do
    print $ (score "example") 
