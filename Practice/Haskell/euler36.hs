import Numeric
import Data.Char
binary = flip (showIntAtBase 2 intToDigit) ""
isPalindrome x = x == reverse x
binPalindromes = sum [n | n <- [1,3..1000000], isPalindrome (show n), isPalindrome (binary n)]

main = do
    print $ binPalindromes
