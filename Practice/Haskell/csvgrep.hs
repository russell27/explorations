import System.Environment
import System.Directory
import Data.Char
import System.IO
import Data.List
import Text.CSV

dispatch :: [(String, [String] -> IO ())]
dispatch = [ ("-h", help), ("-n", getHeader), ("-m", findRecords), ("-i", findRecords'), ("-f", findRecordsIndex), ("-c", selectColumn)]

getRecords :: Either t t1 -> t1
getRecords records = case records of
                          Right records -> records

findRecords [text, fileName] = do
           records <- parseCSVFromFile fileName
           let rec = getRecords records
           mapM_ print [x | x <- rec, (text `elem` x) == True]

findRecords' [text, fileName] = do
            records <- parseCSVFromFile fileName
            let rec = getRecords records
            mapM_ print [x | x <- rec, (text `elem` x) == False, length x > 1]

selectColumn [text, filename] = do
            records <- parseCSVFromFile filename
            let rec = getRecords records
            let indices = map digitToInt $ filter (/=',') text
            let matrix =[transpose(rec) !! x | x <- indices]
            let columns = transpose(matrix)
            print columns
            
findRecordsIndex [int, text, fileName] = do
          records <- parseCSVFromFile fileName
          let rec = getRecords records
          let x = read index :: Int
          mapM_ print [word | word <- rec, text == (word !! x)]

getHeader [fileName] = do
         records <- parseCSVFromFile fileName
         let rec = getRecords records
         print $ rec !! 0 

help [text] = do
    contents <- readFile "help.txt"
    putStr $ contents

main = do
    (command:args) <- getArgs
    let (Just action) = lookup command dispatch
    action args
