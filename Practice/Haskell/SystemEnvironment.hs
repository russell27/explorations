import System.Environment   
import Data.List  

main = do  
    args <- getArgs                  
    progName <- getProgName
    execPath <- getExecutablePath     
    putStrLn "The arguments are:"  
    mapM putStrLn args  
    putStrLn "The program name is:"  
    putStrLn progName
    putStrLn "The program Executable path is:"  
    putStrLn execPath
    [env] <- getArgs
    withFreshDB env "news"
    withArgs [] $ hspec newsSpec