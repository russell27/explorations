import Data.List
import System.Environment
import Text.CSV

sameHeaders :: [[String]] -> [[String]] -> Bool
sameHeaders x y = x !! 0 == y !! 0

intercalateWith :: [String] -> String
intercalateWith = intercalate ","

removeHeader :: [[String]] -> [[String]]
removeHeader x = drop 1 x

removeEmptyList :: [[String]] -> [[String]]
removeEmptyList list = filter (\e -> e/=[""]) list

final :: [[String]] -> [String]
final y = map intercalateWith y

getRecords :: Either t t1 -> t1
getRecords records = case records of
                          Right records -> records

main = do
  args <- getArgs
 
  let arg1 =  args !! 0
  csvFile1 <- parseCSVFromFile arg1
  let file1 = removeEmptyList $ getRecords csvFile1

  let arg2 = args!! 1 
  csvFile2 <- parseCSVFromFile arg2
  let file2 = removeEmptyList $ getRecords csvFile2
  
  if sameHeaders file1 file2
  then mapM_ putStrLn $ final $ file1 ++ (removeHeader  file2)
  else print "No"
  
 
 
