{-# LANGUAGE DeriveDataTypeable #-}                                                                                
module Main where
import Text.Read
import System.Console.CmdArgs 
import Data.Maybe
import System.IO
import Text.Layout.Table
import Data.List
import System.Environment
import Text.CSV
import Text.Parsec.Error
import Data.Either.Unwrap

data Options = Options {
   inputCSVH :: String,
   inputCSV :: String
} deriving (Show, Data, Typeable) 

options::Options
options = Options {inputCSVH = ""
                               &= typ "FILENAME"
                               &= help "ablifying CSV with header"
                   , inputCSV = ""
                               &= typ "FILENAME"
                               &= help "Tablifying CSV files without headers" 
 }

repli :: [a] -> Int -> [a]
repli xs n = concat $ map (replicate n) xs

removeEmptyList :: CSV -> CSV
removeEmptyList list = filter (\e -> e/=[""]) list

calcSizes :: Foldable t => [t a] -> (Int, Int)
calcSizes file = (length (head file) , ((length file) - 1))

main :: IO ()
main = do
    opts <- cmdArgs options
    let csvFile = parseCSVFromFile $ inputCSVH opts
    let file = removeEmptyList $ fromRight csvFile
    let (colCount, rowCount) = calcSizes file
    
    putStrLn $ tableString (repli [numCol] colCount)
                      unicodeRoundS
                    (titlesH (file !! 0))
                       [ rowG (file !! x) | x <- [1..rowCount]]
