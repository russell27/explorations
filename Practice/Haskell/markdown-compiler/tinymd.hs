import System.Environment (getArgs)
import System.IO
import Data.List
import Data.List.Split

attributes = ['`', '_', '*', '~']

convertHeader :: String -> String
convertHeader s = "<h" ++ (show x)  ++ ">" ++ (drop x s) ++ "<h" ++ (show x) ++ "/>"
                             where x = length $ filter (== '#') s

convertText :: String -> String
convertText s | (head s) == '`' = "<code>" ++ q ++ "</code>"
              | (head s) == '_' = "<italic>" ++ q ++ "</italic>"
              | (head s) == '~' = "<strike>" ++ tail(init q) ++ "</strike>"
              | otherwise       = "<strong>" ++ tail(init q) ++ "</strong>"
                             where q = tail(init s)

convert :: String -> String
convert s | head s == '#'              = convertHeader s
          | (head s) `elem` attributes = convertText s
          | nub s == "-"               = "<hr>"
          | head s == '>'              = "<blockquote>" ++ tail s ++ "</blockquote>"
          | otherwise                  = if take 2 (reverse s) == "  " then q ++ "<br>" else q
                             where q = "<p>" ++ s ++ "</p>"

main = do
    args <- getArgs
    let inputfile = head args
    let outputfile = last args
    contents <- readFile inputfile
    let xs = [convert x | x <- (splitOn "\n" contents), x /= ""]
    writeFile outputfile $ unlines xs

