import Text.CSV

howManyRecords :: Foldable t => [t a] -> Int
howManyRecords xs = length $ [x | x <-(tail xs), length x > 1]

main = do
  test_csv <- parseCSVFromFile "car-sales.csv"
  case test_csv of
    Right csv -> print $ howManyRecords csv
    Left err -> print err
