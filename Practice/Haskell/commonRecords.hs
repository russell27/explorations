import Text.CSV
import System.Environment
import Data.Either

commonRecords :: Eq t => [t] -> [t] -> [t]
commonRecords xs ys = [x | x <- xs, y <- ys, x == y]

getRecords :: Either t t1 -> t1
getRecords records = case records of
                          Right records -> records

main :: IO()
main = do
  args <- getArgs

  let arg1 = args !! 0
  records1 <- parseCSVFromFile arg1
  let rec1 = getRecords records1

  let arg2 = args !! 1
  records2 <- parseCSVFromFile arg2
  let rec2 = getRecords records2

  print $ commonRecords rec1 rec2
