import System.Environment (getArgs)
import System.IO
import Data.List.Split

convertHeader :: [Char] -> [Char]
convertHeader s | head s == '#' = "<h1>" ++ " " ++ tail s ++ " " ++ "</h1>"
                | otherwise     = "<p>" ++ " " ++ s ++ " " ++ "</p>"

main = do
    args <- getArgs
    let inputfile = args !! 0
    contents <- readFile inputfile
    let xs = [convertHeader x | x <- (splitOn "\n" contents), x /= ""]
    putStr $ unlines $ xs
