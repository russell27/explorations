isPrefix :: Eq a => [a] -> [a] -> Bool
isPrefix [] ys         = True
isPrefix (x:xs) []     = False
isPrefix (x:xs) (y:ys) = isPrefix xs ys && (x == y)
