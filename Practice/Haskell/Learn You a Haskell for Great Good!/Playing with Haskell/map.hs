map' :: (t1 -> t) -> [t1] -> [t]
map' p []     = []
map' p (x:xs) = p x : map' p xs
