average :: [Int] -> Rational
average [] = error "empty"
average xs = (sum' xs) / toRational (length' xs)
