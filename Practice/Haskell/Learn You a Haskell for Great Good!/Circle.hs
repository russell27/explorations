module Circle
( radius
, diameter
, area
, circumference
) where

radius :: Floating a => a -> a -> a -> a -> a
radius x y x1 y1 = sqrt $ (x - x1) ^ 2 + (y - y1) ^ 2

diameter :: Floating a => a -> a -> a -> a -> a
diameter x y x1 y1 = (radius x  y x1 y1) ^ 2

area :: :: Floating a => a -> a -> a -> a -> a
area x y x1 y1 = pi * (radius x y x1 y1) ^ 2

circumference :: :: Floating a => a -> a -> a -> a -> a
circumference x y x1 y1 = 2 * pi * (radius x y x1 y1)


