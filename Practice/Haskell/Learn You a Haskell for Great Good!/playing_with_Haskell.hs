-- 1. Homemade version of length
length' :: [a] -> Int
length' []     = 0
length' (x:xs) = 1 + length' xs

-- 2. Homemade version of min
min' :: Ord t => t -> t -> t
min' x y | x <= y     = x
         | otherwise  = y   
 
-- 3. Homemade version of max
max' :: Ord t => t -> t -> t
max' x y | x >= y     = x
         | otherwise  = y

-- 4. Homemade version of sum
sum' :: [Int] -> Int
sum' []     = 0
sum' (x:xs) = x + sum' xs

{- 1,4 --> patterns, 2,3 --> guards. Generally, guards 
 - are more readable than patterns.These are similar to 
 - if, else if, else in imperative languages. -}

-- 5. Homemade version of head
head' :: [t] -> t
head' []     = error "empty list"
head' (x:xs) = x

-- 6. Homemade version of tail
tail' :: [t] -> [t]
tail' []     = error "empty list"
tail' (x:xs) = xs

--Haskell has laziness so lists can be used as streams

-- 7. Homemade version of last
last' :: [t] -> [t]
last' []     = error "empty list"
last' (x:[]) = [x]
last' (_:xs) = last' xs

-- 8. Homemade version of init
init' :: [a] -> [a]
init' []        = error "empty list"
init' (x:[])    = [x]
init' (x:xs:[]) = [x]
init' (x:xs)    = x : init' xs

--(x:[]) is nothing but consing x to an empty list i.e. (x:[]) == [x]

-- 9. Homemade version of take
take' :: Int -> [a] -> [a]
take' n []      = error "empty list"
take' 0 xs      = []
take' n (x:[])  = [x]
take' n (x:xs)  = x : take' (n - 1)  xs

-- 10. Homemade version of map
map' :: (t1 -> t) -> [t1] -> [t]
map' p []     = []
map' p (x:xs) = p x : map' p xs

-- 11. Homemade version of filter
filter' :: (t -> Bool) -> [t] -> [t]
filter' p []     = []
filter' p (x:xs) | p x       = x : filter' p xs
                 | otherwise =     filter' p xs

-- 12. Homemade version of reverse
reverse' :: [a] -> [a]
reverse' []     = error "empty list"
reverse' (x:[]) = [x]
reverse' (xs)   = last' xs ++ reverse' (init xs)

-- 13. Homemade version of drop
drop' :: Eq a => Int -> [a] -> [a]
drop' n []  = error "empty list"
drop' n xs  = filter (\x -> x `notElem` ys) xs where ys = take' n xs

{-Here a is type variable and "=>" indicates that it belongs
 - to type class of Eq and "Eq a" is known as class constriant
 - generally, type class is a sort of interface that defines some 
 - behaviour. Like Eq type class provides an interface for testing 
 - for equality. -}
