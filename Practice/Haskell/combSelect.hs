facs = scanl (*) 1 [1..100]
comb (r,n) = facs !! n `div` (facs !! r * facs!!(n-r))
permutations = [(n,x) | x <- [1..100], n <-[1..x]]
combSelections = length $ filter (> 1000000) $ map comb $ permutations

main = do
    print $ combSelections
