{-module Stat
( min'
  max'
  sum'
  mean
) where -}


import System.Environment
import System.Directory
import Data.Char
import System.IO
import Data.List
import Text.CSV
import Data.Char (isDigit)
import Data.List.Split
import Data.Function (on)
import qualified Data.Set as Set
import Data.Ord


-- optional -c OR --column (PART-1 & PART-2) 

-- PART-1
-- Finding whether "cols" field is [Int] i.e [colIndex] / [String] i.e. [ColName]

isNum :: String -> [Bool]
isNum xs = nub [(isDigit x) | x <- (filter (/=',') xs)]

isValid :: String -> Bool
isValid xs = (length (isNum xs)) == 1

-- PART-2
-- getting the columns of given [index] / [colName]

getCols :: String -> [[String]] -> [[String]]
getCols cols xs | (isValid cols) && head (isNum cols) = [transpose(xs) !! n | n <- (map digitToInt $ filter (/=',') cols)]
                | otherwise                           = [transpose(xs) !! n | n <- [length (takeWhile (/= name) (xs !! 0)) | name <- (splitOn "," cols)]]

-- PART-3
-- Checks whether given column contains the numerical values

canApply :: [String] -> Bool
canApply xs = and [head (isNum x) | x <- (tail xs)]

-- PART-4
-- calculating min, max, sum, mean, median, stdev of cols 

toInts :: [String] -> [Int]
toInts xs = [(read x :: Int) | x <- (tail xs)]

min' :: [String] -> Int
min' xs = minimum (toInts xs)

max' :: [String] -> Int
max' xs = maximum (toInts xs)

sum' :: [String] -> Int
sum' xs = sum (toInts xs)

mean :: [String] -> Int
mean xs = (sum' xs) `div` (length xs)

median :: [String] -> Int
median xs | odd (length xs)  = ys !! mid
          | otherwise        = (ys !! mid + ys !! (mid + 1)) `div` 2
                  where mid = (length xs) `div` 2
                        ys = toInts xs

stdev :: [String] -> Int
stdev xs = (sum [(y - m) ^ 2 | y <- ys]) `div` (length ys)
                    where ys = toInts xs
                          m = mean xs 

applyFunction :: ([String] -> a) -> String -> [[[Char]]] -> [a]
applyFunction f cols xs = [f rs | rs <- (getCols cols ys), canApply rs]
                              where ys = [filter (not.null) x | x <- xs]

--optional -n OR --names (gets column names)

getColNames :: [a] -> a
getColNames xs = head xs

-- From here add to main.hs (IO part)

getRecords :: Either t t1 -> t1
getRecords records = case records of
                          Right records -> records

main = do
  args <- getArgs
  let cols = args !! 0
  let filePath = args !! 1
  csvFile <- parseCSVFromFile filePath
  let xs = getRecords csvFile
  print $ applyFunction median cols xs
  --print $ getColNames xs
  
  
--freq in csvstat Only output lists of frequent values.
--unique in csvstat Only output counts of unique values.
-- len Only output the length of the longest values.


freqOfDistinctElem :: Ord a => [a] -> [(a, Int)]

freqOfDistinctElem xs = reverse $ sortBy (compare `on` snd) [(c,length $ filter (== c) xs) | c <- nub xs]


unique :: Ord a => [a] -> [a]

unique = Set.toList . Set.fromList


countOfUniqueElem :: Ord a => [a] -> Int

countOfUniqueElem xs = length $ unique xs


longestLength :: [String] -> Int
longestLength xs = fst $ maximum $ [(length x,x) | x <- xs]