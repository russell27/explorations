import Data.List
import Data.Either
import Text.CSV

getColumnInCSV :: CSV -> String -> Either String Integer
getColumnInCSV csv columnName =
  case lookupResponse of
    Nothing -> Left
      "The column does not exist in this CSV file."
    Just x -> Right (fromIntegral x)
  where
  -- This line looks to see if column is in our CSV
  lookupResponse = findIndex (== columnName) (head csv)

applyToColumnInCSV :: ([String] -> b) -> CSV -> String -> Either
String b
applyToColumnInCSV func csv column = either
    Left
    Right . func . elements
  columnIndex
  where
  columnIndex = getColumnInCSV csv column
  nfieldsInFile = length $ head csv
  records = tail $
    filter (\record -> nfieldsInFile == length record) csv
  elements ci = map
    (\record -> genericIndex record ci) records

main :: IO ()
main = return ()
--main :: IO ()
--main = do
--values <- getArgs parseCSVfile <filename>
--print . getColumnInCSV $ map read values
