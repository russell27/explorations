import Text.CSV

getRecord :: (Foldable t, Eq a) => a -> [t a] -> [t a]
getRecord s xs =  [x | x <- xs, (s `elem` x) == True]

main :: IO()
main = do
  findThis <- getLine
  records <- parseCSVFromFile "car-sales.csv"
  case records of
    Right csv -> print $ getRecord findThis csv
    Left err -> print err
