import System.Environment
import System.Directory
import Data.Char
import System.IO
import Data.List
import Text.CSV

dispatch :: [(String, [String] -> IO ())]
dispatch = [ ("-h", help), ("-n", getHeader), ("-c", getColumn), ("-C", getNotColumn)]

getRecords :: Either t t1 -> t1
getRecords records = case records of
                          Right records -> records
                          
getColumn [text, filename] = do
            records <- parseCSVFromFile filename
            let rec = getRecords records
            let indices = map digitToInt $ filter (/=',') text
            let matrix =[transpose(rec) !! x | x <- indices]
            let columns = transpose(matrix)
            print columns

getNotColumn [text, filename] = do
            records <- parseCSVFromFile filename
            let rec = getRecords records
            let v = length rec
            let notIndices = [x | x <- [0..v-1], x `notElem` indices]
            let matrix =[transpose(rec) !! x | x <- notIndices]
            let columns = transpose(matrix)
            print columns

getHeader [fileName] = do
         records <- parseCSVFromFile fileName
         let rec = getRecords records
         print $ rec !! 0 

help [text] = do
    contents <- readFile "help.txt"
    putStr $ contents

main = do
    (command:args) <- getArgs
    let (Just action) = lookup command dispatch
    action args
