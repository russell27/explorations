module Main where

add x y = x + y

addAll [] = 0
addAll [x] = x
addAll (x:xs) = add x (addAll xs)

process xs = show $ addAll $ map read xs

main = do
  putStrLn "Give me a number"
  val1 <- getLine
  putStrLn "And another number"
  val2 <- getLine
  putStrLn "One more, please"
  val3 <- getLine
  putStrLn $ "The sum is: " ++ process [val1, val2, val3]

