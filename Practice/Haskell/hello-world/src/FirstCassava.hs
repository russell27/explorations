
import Control.Applicative
import qualified Data.ByteString.Lazy as BL
import Data.Csv
import qualified Data.Vector as V

data Food = Food
    { food_name   :: !String
    , scientific_name :: !String
    , group :: !String
    , sub_group :: !String
    }

instance FromNamedRecord Food where
    parseNamedRecord r = Person <$> r .: "food_name" <*> r .: "group"


main = do
    csvData <- BL.readFile "generic-food.csv"
    case decodeByName csvData of
        Left err -> putStrLn err
        Right (_, v) -> V.forM_ v $ \ p ->
            putStrLn $ food_name p ++ " belongs to " ++ group p