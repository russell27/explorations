{-# LANGUAGE DeriveDataTypeable #-}
module Main where
import Text.Read
import System.Console.CmdArgs 
--import Control.Monad
--import Data.Char
import Data.List
--import System.Console.GetOpt
--import System.Environment
--import System.Exit
import Data.Maybe
import System.IO
--import Text.Printf

{--parseInput :: String -> Maybe Int
parseInput input = if input == "exit" then Nothing else (readMaybe input):: Maybe Int


even_only :: [Int]->[Int]
even_only [] = []
even_only (x:xs)
    | is_even x = x:(even_only xs)
    | otherwise = even_only xs
    where
        is_even :: Int -> Bool
        is_even x = (mod x 2) == 0--}

factorial :: ( Integral a ) => a -> a
factorial 0 = 1
factorial n = n * factorial ( n - 1)

data Options = Options {
  factorialOf :: Int
} deriving (Data, Show)

options::Options
options = Options {factorialOf = 1
                              &= typ "LIST"
                              &= help "Filters even numbers from a given list"

}

main :: IO ()
main = do
  opts <- cmdArgs options

  let fac = factorial $ factorialOf opts
  print $ fac


