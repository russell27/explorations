factors n = [i | i <- [2..n], n `mod` i == 0]
isPrime n = (length $ factors n) == 1
lucas = 2 : 1 : zipWith (+) lucas (tail lucas)
primeLucas = filter isPrime lucas

main = do
    print $ take 10 primeLucas
