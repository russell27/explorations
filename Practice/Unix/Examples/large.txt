Samples Essays and Commentary
CONTACT INFORMATION
All correspondence and mailings should be addressed to:
CaMLA
Argus 1 Building
535 West William St., Suite 310
Ann Arbor, Michigan
48103-4978 USA
T: +1 866.696.3522
T: +1 734.615.9629
F: +1 734.763.0369
info@cambridgemichigan.org
www.CambridgeMichigan.org
© 2013 Cambridge Michigan Language Assessments®
MELAB Sample Essays and Commentary 1
Contents
On the following pages are ten MELAB essays
representative of each score on the MELAB Writing
Rating Scale. Commentaries follow each sample
essay. A condensed version of the rating scale is
available on page 22.
Essay 1 Rating: 97...................................................2
Commentary...............................................3
Essay 2 Rating: 93...................................................4
Commentary...............................................5
Essay 3 Rating: 87...................................................6
Commentary...............................................7
Essay 4 Rating: 83...................................................8
Commentary...............................................9
Essay 5 Rating: 77.................................................10
Commentary.............................................11
Essay 6 Rating: 73.................................................12
Commentary.............................................13
Essay 7 Rating: 67.................................................14
Commentary.............................................15
Essay 8 Rating: 63.................................................16
Commentary.............................................17
Essay 9 Rating: 57.................................................18
Commentary.............................................19
Essay 10 Rating: 53.................................................20
Commentary.............................................21
MELAB Writing Rating Scale..................................22
MELAB Sample Essays and Commentary 2
Essay 1 Rating: 97
Technology has highly evolved over time. In fact, nowadays almost
everybody has some sort of machine at hand, be it computers, cars,
or even washing machines. But although machinery was devised to
benefit mankind, it has also brought along many flaws to match.
Firstly, when it comes to technological equipment such as
computers, disruption most often arises between the person using
the computer, and the household he or she is surrounded by, or
living with. For example, many old family traditions such as eating
meals with your relatives at the dining table seldom take place now
that one of the family members might be too busy working on his
Mac. laptop. Thus, family values and morals have changed in order to
adapt to this technological age.
Secondly, having many kinds of machinery at hand is not
only destroying family traditions, but is also very harmful to our
environment. Many people are careless about allowing their car
engines to run haphazardly, or leaving their laptops on for long
periods of time, however they do not seem aware of the fact that all
this energy and electricity consumption is dangerous to not only our
local environment, but to the world as a whole. Additionally, it is the
over-usage of machinery, big or small that is bringing our society ever
closer to Global Warming, and we must stop.
As I mentioned in my introduction, technological equipment was
never programmed to damage nature per se, but to help people
all around the globe. Now that nuclear families aren’t as closely
intact compared to the 1950s or 1990s, technology has given us an
alternative method to keep in touch with our relatives thanks to
computer applications such as Skype, or even cell phone applications
such as VIBER or WhatsApp. Machinery has most definitely done
wonders in our lives, and we as people should be grateful to easily
possess cars, and/or phones when poorer countries do not even have
the chance to.
However, with all the advantages machinery has brought to us all,
I personally believe that possessing too many cars or phones, or even
consuming too much of their energy and battery, is beginning to get
out of hand and needs to be controlled.
MELAB Sample Essays and Commentary 3
Commentary on Essay 1
The writing prompt this test taker wrote on was:
Machines now play an important role in most people’s lives. Computers, cars, and
household machines (such as washing machines) have become very common. What are the
advantages and disadvantages of having so many machines in people’s lives? Use specific
examples in your answer.
Essay 1 would be awarded a score of 97. It is a
polished piece of writing that thoroughly develops
the topic of the prompt, far beyond the superficial
level, and acknowledges its complexity.
Syntactic control is very strong in this essay. The
writer demonstrates consistent control over complex
and varied syntactic structures, and the response
is essentially error free. There is a wide range of
appropriately used vocabulary.
The essay is also very cohesive. The writer builds the
argument not through a reliance simply on the use of
mechanical transition markers, but in a much more
sophisticated way, developing each supporting point
richly and connecting the ideas together so that the
essay reads smoothly. It is very easy for the reader to
follow the development of the ideas in the text.
MELAB Sample Essays and Commentary 4
Essay 2 Rating: 93
It is true that medical doctors are health professionals whose roles are of
great importance in the society. They are usually educated and people generally
look up to them to take good care of their health issues. In fact, whether people
live or die depends to a very great extend on the doctors in charge of their health
situations, depending on how serious they are. Almost every one wants a doctor
that is vastly knowledgeable in the medical field, especially those with many years of
experience.
Although I agree that the level of education of a medical doctor is of much
importance for a doctor to be regarded as a “good medical doctor”, I am also of
the opinion that that is not the only criterion for a doctor to be regarded as a “good
medical doctor”. Character and the integrity of the person contribute to a large
extend. I am also of the view that a medical doctor should be a very responsible
person in the society because alot of people tend to emulate them. Young adults
usually look forward to becoming medical doctors in the future. That being the case,
they tend to copy the behaviours of medical doctors they know.
The country where I come from, Nigeria, where the health system has some
issues, some medical doctors who lack good character and do not care about their
integrity can, because a patient does not have enough money to pay their medical
bills, walk away from a dying patient. During oath taking, these doctors state that
they will put patients first before any thing, but some of them do not keep up with
this. Although I agree that doctors in the third world countries usually face some
challenges, I am still of the opinion that when it comes to saving a patient’s life, a good
medical doctor should always swing into action. In Canada, though I have stayed here
for just a year, I think many of their doctors can be regarded as “good doctors”
from the little I have seen.
In conclusion, I believe that a well educated doctor without a good character
and integrity and also without a passion to save lives is as good as a “bad doctor”
since, with his education but with love for money rather than love for his patient’s
lives, can still lead to the loss of lives.
MELAB Sample Essays and Commentary 5
Commentary on Essay 2
The writing prompt this test taker wrote on was:
Different people value different qualities in a medical doctor. Everyone wants a welleducated and knowledgeable doctor, but what other qualities should a good medical doctor
have? Explain why these are important.
Essay 2 would be awarded a score of 93. The writer
has produced a lengthy and on-target response that
is very readable. The essay effectively addresses the
complexity of the prompt.
The writer employs an impressively wide range of
syntactic structures flexibly to build coherence and
cohesion and create a fluent style that includes
control of emphasis.
The writer has generally good control over syntax
and vocabulary. There are a few cases of slightly
awkward vocabulary use (“vastly knowledgeable,”
“do not keep up with this”) and a number of minor
errors with mechanics that detract slightly from the
overall impression created by this excellent essay.
MELAB Sample Essays and Commentary 6
Essay 3 Rating: 87
Traveling and visiting new countries can be an extremely thrilling
experience. I usually prefer to take guided tours to explore a new
destination. By doing that I get a chance to visit its most significant
landmarks, get to meet tourists from other countries and learn about
the city’s most popular and authentic restaurants.
Upon joining professional excursions, tourists discover the prominent
cultural and historical buildings in cities, such as churches, monuments,
parks, and palaces. The tours are usually organized to cover most of
these important places. For example, when I visited Dubai last year, I
joined the “Big Bus Tour” to explore this amazing city. While visiting the
Dubai Musuem, I learned that this chief city was only built sixty years
ago. Haven’t I taken such a guided tour, I wouldn’t have known this fact
about this metropolis city.
The other reason to prompt me in taking tour buses is the
opportunity to meet other tourists. sharing the experience and spending
time together gets you close to other people from different nationalities.
This in return provides you with the chance of learning about other
cultures and maybe building frienships with them. As an example, while
touring in Mexico two years ago, I was acquainted to tourists from
various cultures and learned a lot about each other.
Another important factor to encourage me on taking bus tours is
learning about the city’s most popular restaurants. Although brochures
and flyers usually contain such information, yet tour guides who live in
they cities recommend more authentic local places. As an example, one of
the best meals I tried was at a local restaurants in Mexico City where
the guide advised us to go. It was a meal to remember.
In conclusion, taking professional excursions is worth the cost of it.
You get to visit all the salient landmarks, learn about other cultures
and get familiar with popular restaurants.
MELAB Sample Essays and Commentary 7
Commentary on Essay 3
The writing prompt this test taker wrote on was:
When visiting a new city or country, some people like to go on group tours led by
professional tour guides. Others prefer to explore new places on their own. Which do you
prefer? Give reasons and examples to support your opinion.
Essay 3 would be awarded a score of 87. The topic
development for this essay is very good. The writer
provides a clear opinion and a large amount of
supporting detail within the response. The content is
directly relevant to the prompt.
The writer demonstrates consistent control
over simple sentence structures. More complex
constructions are not always successful (“Haven’t
I taken such a guided tour”), but the errors are
relatively few and not distracting. The vocabulary
used in the response is prompt specific but there
is not a lot of low-frequency vocabulary produced.
Where low-frequency vocabulary is used it is not
always correct (“acquainted to”).
Overall, this essay was awarded a score of 87
because of its excellent topic development and
control over sentence structure. It does not merit
a higher score because of the quantity of error and
limited use of low-frequency vocabulary.
MELAB Sample Essays and Commentary 8
Essay 4 Rating: 83
Working Part-time Jobs
Part-time jobs are very common nowadays. I, myself, work part-time
since I have to go to school part-time as well. I think a lot of people
opt for working part-time because of time flexibility and we would still be
able to attend to other things that are important in our day to day
living.
A clear advantage of having several part-time jobs is for people to be
developed in the different areas where they are working. If one has several
jobs, he or she will be able to meet several people and supervisors that
will expose them to different challenges and learnings. Another advantage of
having several part-time jobs, is an opportunity to find out what you really
want as an occupation in life. This may be a stepping stone for you to be
able to discover yourself more in terms of your abilities and skills. Lastly,
I think having several jobs will make you earn more money than having a
full-time job. This is because in a full-time job, you are only entitled for
a specific number of hours at work and sometimes, employers don’t give
overtime pays.
The disadvantage on the other hand, is that most of the part-time
jobs do not offer benefits to its employees. Also, you have to adjust with
your workmates from one workplace to another. It will also be difficult
if you have two or three jobs in a day and you don’t have your own car.
Commuting from one workplace to another can be very exhausting.
In conclusion, I would say that I’m in favor of having several parttime jobs rather than one full-time job. My decision is mainly in line with my
current situation. I need to go to school and at the same time, work for
my family. Having part-time jobs will be very beneficial for me since I can
arrange my schedule with my employer. Also, based on my own experience, I
enjoy meeting and working with people from different walks of life. I was
able to gain friends which I think is very essential to be able to adapt
easily in a country that is different from yours.
MELAB Sample Essays and Commentary 9
Commentary on Essay 4
The writing prompt this test taker wrote on was:
In many countries it is becoming more common for people to have several part-time jobs
instead of one full-time job. What are the advantages and disadvantages of working parttime jobs? Give examples to support your ideas.
Essay 4 would be awarded a score of 83. The essay
is very straightforward and easy to understand, and
it addresses both sides of the topic successfully.
The writer clearly separates different points
into distinct paragraphs, and the organizational
pattern is transparent to the reader, contributing
to its readability. A limitation is the relatively thin
development. The second paragraph is convincing,
but after that, disadvantages are listed with only
limited support and discussion.
The range and accuracy of the syntactic structures
and vocabulary contribute to the development of
ideas. Sentences are varied in length and complexity;
vocabulary is broad and generally accurately used. 
MELAB Sample Essays and Commentary 10
Essay 5 Rating: 77
With the developement of our society, people’s lifestyle change from day
to day. It has become more common for people to have different part-time jobs.
Compared this trend with before, some people start wondering whether it is good or
bad for people to have many jobs at the same time. However, in my opinion, I think
part-time job is a double sides sword. It has its own advantages and disadvantages.
For some people, part-time jobs provide them an oppotunity to do things they are
actually interested in. Also, it offers the possibility to earn more money at the same
time. But for other people, part-time jobs are time-consuming and they cost many
energy to do so many jobs at the same time period.
The most important advantage of part-time jobs is that they provide an
oppotunity for people to try different things. Taking my brother as an example,
he works in a bank as a teller, but he has seen many videos about bartender, so he
decides to be a bartender at night. This job extends his sight and provides him a
chance to fulfill his dream.
Speaking of dreams, part-time jobs also offer people a chance to earn more
money. Part-time jobs are the best path for someone who needed money. Although
it takes time, people could still benefit from the work they did. In our society, many
people take part-time jobs in order to make enough money to pay bills or pay for
their kids tuition.
However, as we all know, it takes time and energy to do so many jobs. This is the
most disadvantage of part-time jobs. People get tired and stressed because of their
work. They don’t have time with families. All they want to do is finishing the job and
sleep. If a father takes several part-time jobs, it may influence on his children. His
children may lack of love and they may become a workacholic person in the future.
Taking all these factors into account, we may draw a conclusion that part-time
can do good things in your life, but it can also destroy your time in life. We may have
to think about what are we pursuing in our lives before we bury into the work. Money
is important, but family and life are much worth for us. Time is not coming back.
MELAB Sample Essays and Commentary 11
Commentary on Essay 5
The writing prompt this test taker wrote on was:
In many countries it is becoming more common for people to have several part-time jobs
instead of one full-time job. What are the advantages and disadvantages of working parttime jobs? Give examples to support your ideas.
Essay 5 would be awarded a score of 77. It is a
cohesive piece of writing that is well organized and
on topic. The writer has the linguistic and rhetorical
resources to address both the advantages and
disadvantages of the topic and end with a logical
conclusion.
The writer uses both simple and complex syntactic
structures, but the control of syntax is uneven.
Some sentences are error free but others contain
distracting morphological mistakes (“people’s
lifestyle change,” “Compared this trend with before,”
“someone who needed money”).
The vocabulary is adequate to convey the author’s
ideas clearly but is not always appropriately used.
The essay has instances of successful use of lexical
phrases (“it takes time and energy”) and others
where vocabulary is awkward or incorrect (“extends
his sight”).
Overall, this essay is a clear 77. Development is
good, and the whole text builds logically, although
uneven control of syntax and vocabulary detracts
from the overall impression created by the essay.
MELAB Sample Essays and Commentary 12
Essay 6 Rating: 73
I agree the government policy of all of the citizens should learn
two foreign languages. The world is getting more closer than before.
It is very easy for us to travel to another country. Students can go
to another country for study. Business man can find a parnet from
other country to improve thir business. Also, there are more and
more information can find on the internet. All of these are base on
languages.
There are more and more students travel to another country for
study. They travel to another country because they want learn other
language. If the students can speak more languages, then they can
talk to someone easier, and also can learn more. Also, they can make
some friends easily. These are the advantages for learing more
languages.
International business is getting common. There are more and
more companines located in whole world. If the boss of the company
can speak more than one language, then the boss can find some
parnets from different countries, then the boss can have a successful
business easily. Learning two languages is also good for the people
who is working.
There are more and more information can find on the internet.
Some people will translate the information into their language. But
sometimes they will lose some important information. If we can know
other language, then we can find more resources for the information,
and also more detail. Learning more languages can also learn more
knowledges.
Learning two or more languages is good for us. We can go to
other country for study, boss of the company can try to improve the
company becomes a international company, and also we can learn more
on the internet. There are the advantges of learning more than one
languages. So I agree the government policy of all of citizens should
learn two foreign languages.
MELAB Sample Essays and Commentary 13
Commentary on Essay 6
The writing prompt this test taker wrote on was:
In some countries, the government policy is that all citizens should learn two foreign
languages. Do you think this is a reasonable policy? Or do you think that other subjects
should be a priority in school? Please use reasons and examples to support your opinion.
Essay 6 would be awarded a score of 73. The
response is directly relevant to the prompt, and
the topic is developed with an introduction, several
supporting reasons and examples, and a conclusion.
The writer generally relies on simple sentence
structures. Limitations in syntactic range make
connections between sentences choppy. The reader
can easily derive meaning from the essay, but errorfree sentences are rare.
However, although errors are frequent, they do not
prevent the reader from understanding the text
as a whole. Overall, this essay is a clear 73. It is
adequately developed and organized, and meaning
is clear. However, the degree of inaccuracy of syntax
and vocabulary means it cannot be awarded a
higher score.
MELAB Sample Essays and Commentary 14
Essay 7 Rating: 67
It is widely accepted that most people want their doctors who had graduated
famous university and well-educated. It is associated with people’s life directly being
treated perfectly. However, there are more important factors which affect people’s
condition in the hospital. The first thing is ability of explaination about patient’s
condition and progress. Whenever we are being admitted to the hospital, we are
usually afraid of our next step. Besides, after we experienced big operation or
procedure, we want to know our current state and how to cope with the future.
Even though the doctors are very knowledgeble and skillful, most patients feel scare
when they do not being supported by doctor’s explaination.
Secondly, doctors should be warm.
It is very important to treat the patient’s inner side. It could be huge
encouragement people those who are suffer with incurable disease.
We can not live without human beings.
By time goes by, our society have changed like factory. To sum up, doctors who
are living in modern society, should have warm heart and treat the patient like their
family.
MELAB Sample Essays and Commentary 15
Commentary on Essay 7
The writing prompt this test taker wrote on was:
Different people value different qualities in a medical doctor. Everyone wants a welleducated and knowledgeable doctor, but what other qualities should a good medical doctor
have? Explain why these are important.
Essay 7 would be awarded a score of 67. The
writing performance in this response is uneven. The
language in the first paragraph indicates that the
writer has control over sentence level syntax and is
able to connect sentences together in a meaningful
way. After the first paragraph, the writer loses control
over organizing ideas, and the essay becomes
disjointed. Topic development is incomplete.
The language (syntax and vocabulary) used in the
first paragraph is more complex and accurate than
typically seen in essays rated 67. Intersentential
marking devices are well-used, and the first
paragraph is cohesive, despite some grammar errors.
However, after the first paragraph, there is much less
control over syntax, and there is little connection
of ideas across sentences. The reader needs to
reformulate meaning in order to understand this part
of the composition. The writer is not able to perform
consistently at the level demonstrated in the first
paragraph.
MELAB Sample Essays and Commentary 16
Essay 8 Rating: 63
People say that water is the original energy. We can see it everywhere
and we use it everyday It seems like everything need water. for their life,
as energy of large number
Water always play a good role in our world. We use water clean the
clothes , people need drink water to be energy for them., Sometimes Water
of river and ocean can be a transportation to help people transport
the woods we need the good role everyday. for everyone as the most
important energy.
But everything has two sides. Water also has the bad side
sometimes like when the rain keep a long time as usual of the beginning
it will be a disaster to break people’s nomal life and houses. If we didn’t
cherish water environment They will turn dirty and become pollution then
we couldn’t drink it.
At last. Water give us life and available to use we should grateful
for it. and save water everytime Trying to protect water clean. 
MELAB Sample Essays and Commentary 17
Commentary on Essay 8
The writing prompt this test taker wrote on was:
There are many different energy sources (for example, sun, wind, oil water, natural gas, coal,
nuclear fission, etc.). Pick one such source and discuss its advantages and disadvantages.
You might consider availability, safety, geographical location, etc.
Essay 8 would be awarded a score of 63. The
response is relevant to the prompt but development
is very limited. The writer demonstrates some
awareness of organizational knowledge, as there is
an attempt to build supporting paragraphs from the
introduction and then a short conclusion.
Although the essay demonstrates developing
awareness of organizational knowledge, reader effort
is required to process most of the sentences for
meaning. Simple syntactic structures are present but
with many errors that obscure meaning. Vocabulary
is often inaccurately used. Punctuation errors are
frequent and significant.
Overall, this essay is clearly a 63 because although
there is some control over organization, errors
predominate, and the sentences still require
rereading and considerable reader effort to process
for meaning.
MELAB Sample Essays and Commentary 18
Essay 9 Rating: 57
Alot of people and any body in the world can’t life with out water,
Because have many reson to this why. I will try to informaiton some reson
in this topic. The first reson since build on development in the life the water
is availbilty Because that is very important to life and do anything in this
life. The water is avilbilty ander the land and come in the sky also the god
is who put the water in the world. Should to safe this source. Althou from
alot of this Source in the world. the water have many advantages the
flower and the animal used this Source every day.
the secund thing alot of people life with water in home and when we
need clean something and used it in the day or every day that is good for
them and
all of all the water we can’t any body life without water. I hope
the TV. and news peaper watch or write something for learn the people who
can safe the water. 
MELAB Sample Essays and Commentary 19
Commentary on Essay 9
The writing prompt this test taker wrote on was:
There are many different energy sources (for example, sun, wind, oil water, natural gas, coal,
nuclear fission, etc.). Pick one such source and discuss its advantages and disadvantages.
You might consider availability, safety, geographical location, etc.
Essay 9 would be awarded a score of 57. The essay
is unusually long for a response at this score point,
but it is difficult to understand what the writer is
trying to say. There is little syntactic control; the
writer is not able to produce simple sentences.
Vocabulary is used incorrectly.
The writer has attempted to organize the response
with the use of discourse markers (“The first reason,”
“the second thing”) but these phrases do not really
assist the reader in deriving meaning from the
composition.
Although the topic is developed in a way that is
generally related to the prompt, the language is so
difficult to process for meaning that it cannot be
awarded a score any higher than 57.
MELAB Sample Essays and Commentary 20
Essay 10 Rating: 53
Safety Sun
The sun in summer very hot on diffenert city. Safety sun ware sunglasses with
cap because the broblem eyes with haedeache. Keeb the liett colore the t.shert don’t
ware the durek colore because the liett colore wiathe and yallow don’t keed or save hot
suning.the durak colore black this. is save hot suning. summer a’everybady go to sea
because this is cold sea. in baeche everybody ware tank with shoret. After swimming
in sea don’t sit in sun because the durek skin. The safety after swimming keeb shawer
in batheroom. because sea in more solg the solg keeb the durek skin.
MELAB Sample Essays and Commentary 21
Commentary on Essay 10
The writing prompt this test taker wrote on was:
There are many different energy sources (for example, sun, wind, oil water, natural gas, coal,
nuclear fission, etc.). Pick one such source and discuss its advantages and disadvantages.
You might consider availability, safety, geographical location, etc.
Essay 10 would be awarded a score of 53. The writer
may have misunderstood the prompt, but this is an
original piece of writing. It is clearly not rehearsed, so
it is an attempted response to the prompt. The essay
is quite long compared to many responses at this
score point.
Even at the sentence level, it is very difficult for a
reader to derive any meaning from the response.
The writer is not able to connect words together to
produce a meaningful sentence.
There is no control over paragraphing, but the
writer can use punctuation and capitals to indicate
sentence boundaries. Spelling is distracting,
with many words at first seeming indecipherable.
The reader has to guess at the meaning of many
misspelled words (“liett,” “durek,” “shoret”).
MELAB Sample Essays and Commentary 22
07/2013
MELAB Composition Descriptions
97 Topic is richly and fully developed. Flexible use of a wide
range of syntactic (sentence-level) structures, and accurate
morphological (word forms) control. Organization is
appropriate and effective, and there is excellent control
of connection. There is a wide range of appropriately used
vocabulary. Spelling and punctuation appear error-free.
93 Topic is fully and complexly developed. Flexible use of a
wide range of syntactic structures. Morphological control
is nearly always accurate. Organization is well controlled
and appropriate to the material, and the writing is well
connected. Vocabulary is broad and appropriately used.
Spelling and punctuation errors are not distracting.
87 Topic is well developed, with acknowledgment of its
complexity. Varied syntactic structures are used with
some flexibility, and there is good morphological control.
Organization is controlled and generally appropriate to
the material, and there are few problems with connection.
Vocabulary is broad and usually used appropriately. Spelling
and punctuation errors are not distracting.
83 Topic is generally clearly and completely developed, with
at least some acknowledgement of its complexity. Both
simple and complex syntactic structures are generally
adequately used; there is adequate morphological control.
Organization is controlled and shows some appropriacy
to the material, and connection is usually adequate.
Vocabulary use shows some flexibility, and is usually
appropriate. Spelling and punctuation errors are sometimes
distracting.
77 Topic is developed clearly but not completely and without
acknowledging its complexity. Both simple and complex
syntactic structures are present; in some 77 essays, these
are cautiously and accurately used while in others there
is more fluency and less accuracy. Morphological control
is inconsistent. Organization is generally controlled,
while connection is sometimes absent or unsuccessful.
Vocabulary is adequate but may sometimes be
inappropriately used. Spelling and punctuation errors are
sometimes distracting.
73 Topic development is present, although limited by
incompleteness, lack of clarity, or lack of focus. The topic
may be treated as though it has only one dimension, or
only one point of view is possible. In some 73 essays,
both simple and complex syntactic structures are present,
but with many errors; others have accurate syntax but
are very restricted in the range of language attempted.
Morphological control is inconsistent. Organization is
partially controlled, while connection is often absent or
unsuccessful. Vocabulary is sometimes inadequate, and
sometimes inappropriately used. Spelling and punctuation
errors are sometimes distracting.
67 Topic development is present but restricted, and often
incomplete or unclear. Simple syntactic structures
dominate, with many errors; complex syntactic structures,
if present, are not controlled. Lacks morphological control.
Organization, when apparent, is poorly controlled, and
little or no connection is apparent. Narrow and simple
vocabulary usually approximates meaning but is often
inappropriately used. Spelling and punctuation errors are
often distracting.
63 Contains little sign of topic development. Simple syntactic
structures are present, but with many errors; lacks
morphological control. There is little or no organization,
and no connection apparent. Narrow and simple vocabulary
inhibits communication, and spelling and punctuation
errors often cause serious interference.
57 Often extremely short; contains only fragmentary
communication about the topic. There is little syntactic
or morphological control, and no organization or
connection are apparent. Vocabulary is highly restricted
and inaccurately used. Spelling is often indecipherable and
punctuation is missing or appears random.
53 Extremely short, usually about 40 words or less;
communicates nothing, and is often copied directly from
the prompt. There is little sign of syntactic or morphological
control, and no apparent organization or connection.
Vocabulary is extremely restricted and repetitively used.
Spelling is often indecipherable and punctuation is missing
or appears random.
0 A zero can be given to a nonresponse. A completely blank
answer sheet or simply the test taker’s name on the space
where the essay should be written.
A zero can also be given to a composition that is written on
a topic different from any of those assigned. Connection
of composition to the prompt may be so loose that the
essay could very well have been prepared in advance.
Considerable effort must be made to see the connection
between the composition and the prompt.
Writing
Rating Scale
