
	The cat (short for “concatenate“) command is one of the most frequently used command. cat command allows us to create single or multiple files, view contain of file, concatenate files and redirect output in terminal or files. In this article, we are going to find out handy use of cat commands with their examples in Linux.


	BASIC SYNTAX : cat [option] [files]


BASIC COMMANDS : 

	* cat [file-name] : shows the content of the file.

	
	* cat [file1 file2] : shows multiple files.
	
	
	* cat >file-name : Create a new file and you and write over there to save and exit CTRL + d


	*  cat -n [file-name] : Display numbers of each line
	
	
	* cat -e [file-name] : Display $ at end of file


	* cat -T [file-name] : Display tab seperation, it is filled with ^I

	
	* cat [file-input-taken-from] > [file-to-be-overwritten] : Redirecting the file output to another file, if the other file is present it is overwritten else new file is created and written here.


	* >> or << will append to the existing file rather than rewriting the file.


	cat test test1 test2 > test3 : Redirecting from many files to one file.



	SPECIAL CASE :	cat If file having large number of content that won’t fit in output terminal and screen scrolls up very fast, we can use parameters more and less with cat command as show above.
	
	Syntax :

	 cat [file-name] | more
         cat [file-name] | less

