
import argparse


parser = argparse.ArgumentParser(description = 'Example for the argument Parser')
parser.add_argument('blog', help="Best blog name here.")
parser.add_argument('hello', help="Hello World")
parser.add_argument('-w', '--world', help="Best blog name here.")
parser.add_argument('-f', '--fun', help="fun :)")
args = parser.parse_args()

if args.blog == 'JournalDev':
    print('You made it!')

if args.writer == 'Shubham':
    print('Technical Author.')
