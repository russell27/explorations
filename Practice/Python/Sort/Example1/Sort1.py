csv_file = '/home/krishna/fruits.csv'
f_obj = open(csv_file)
f = f_obj.read()
f_fruits = (f.split('\n')[0]).split(',')
f_price = (f.split('\n')[1]).split(',')
dictionary = dict(zip(f_fruits,f_price))
sort_dict = sorted(dictionary.iteritems(), key= lambda t: t[0])
line_1 = [x[0] for x in sort_dict]
line_2 = [x[1] for x in sort_dict]
s1 = ','.join(line_1)
s2 = ','.join(line_2)
print(s1,s2)

