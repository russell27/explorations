extern crate csv;
extern crate argparse;
use std::io;
use argparse::{ArgumentParser,StoreTrue};
use std::error::Error;
use std::collections::HashMap;

fn main() -> Result<(),Box<dyn Error>> {
   let mut headers = false;
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut headers)
            .add_option(&["-n", "--headers"],StoreTrue,"names");
        ap.parse_args_or_exit();
    }

    if headers{
        let mut reader = csv::ReaderBuilder::new().flexible(true).from_reader(io::stdin());
        let headers = reader.headers()?;
        let total_no_of_columns = headers.len();
        let mut row_index = 0;
        let mut found_columns = HashMap::new();
        for result in reader.records(){
                let result = result?;
                row_index = row_index + 1;
                let no_of_columns_in_row = result.len();
                if total_no_of_columns != no_of_columns_in_row{
                    found_columns.insert(row_index,no_of_columns_in_row);
                }
        }
        if found_columns.len() != 0{
            for (row_index,no_of_columns) in &found_columns{
                println!("Line {} : Expected {} columns,found {}",row_index,total_no_of_columns,no_of_columns);
        }}
        else{
            println!("No errors");
    }
    }

    Ok(())
}

