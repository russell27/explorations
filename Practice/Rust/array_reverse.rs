fn main() {
    let cin = std::io::stdin();
    let mut a = String::new();
    println!("Enter the elements:");
    cin.read_line(&mut a).unwrap();
    let vals = a.split_whitespace().map(|x| x.parse::<u32>()).collect::<Result<Vec<u32>, _>>().unwrap();
    let y: Vec<_> = vals.into_iter().rev().collect();
    println!("{:?}",y);
}

