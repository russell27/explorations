use std::collections::HashMap;

fn main() {
    let words = vec![String::from("alpha"), String::from("beta"), String::from("delta"), String::from("alpha")];

    let mut frequency: HashMap<&str, u32> = HashMap::new();
    for word in &words { // word is a &str
        *frequency.entry(word).or_insert(0) += 1;
    }
    println!("{:?}", frequency);
}
