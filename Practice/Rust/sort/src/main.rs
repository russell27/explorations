extern crate argparse;
extern crate csv;
extern crate quickersort;
use argparse::{ArgumentParser, Store};
use std::collections::HashMap;
use std::error::Error;
use std::io;
use std::iter::FromIterator;
use std::ops::Not;
use std::result::Result;
type Record = HashMap<String, String>;
fn main() -> Result<(), Box<dyn Error>> {
    let mut column = 1;
    let mut name = String::new();
    let mut reverse = String::new();
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut column)
            .add_option(&["-c", "--column"], Store, "column");
        ap.refer(&mut name)
            .add_option(&["-n", "--name"], Store, "name");
        ap.refer(&mut reverse)
            .add_option(&["-r", "--reverse"], Store, "reverse");
        ap.parse_args_or_exit();
    }
    let mut csv_read = csv::Reader::from_reader(io::stdin()); 
    let mut keys = Vec::new();
    {
        let _header = csv_read.headers()?; 
    }
    let mut data = Vec::new();
    for i in csv_read.deserialize() {
        let record: Record = i?;
        data.push(record);
    }
    let k = csv_read.headers()?;
    for rec in k.iter() {
        keys.push(rec);
    }
    for i in 0..keys.len() {
        if keys[i] == name {
            column = i + 1
        }
    }
    let mut all_values = Vec::new();
    let mut cols = Vec::new(); 
    for key in keys.iter() {
        let mut values = Vec::new();
        for i in data.iter() {
            for (k, val) in i {
                if k == key {
                    all_values.push(val);
                    values.push(val);
                }
            }
        }
        cols.push(values); 
    }
    let mut rows = Vec::new(); 
    for c in 0..cols[0].len() {
        let mut r = Vec::new();
        for x in (c..all_values.len()).step_by(cols[0].len()) {
            r.push(all_values[x]);
        }
        rows.push(r); 
    }
    let mut wtr = csv::Writer::from_writer(io::stdout());
    wtr.write_record(&keys)?; 
    let mut required_col = Vec::from_iter(cols[column - 1].iter().cloned());
    let mut index = Vec::new();
    let unsorted_col = Vec::from_iter(required_col.iter().cloned());
    let first_value = required_col[0];
    if first_value.parse::<u32>().is_ok() {
        required_col.sort_by_key(|x| x.parse::<u32>().unwrap());
    } else {
        required_col.sort_by(|a, b| a.cmp(b));
    }
    let sorted_col = Vec::from_iter(required_col.iter().cloned());
    for i in 0..sorted_col.len() {
        for j in 0..unsorted_col.len() {
            if sorted_col[i] == unsorted_col[j] && index.contains(&j).not() {
                index.push(j);
                wtr.write_record(&rows[j]); 
            }
        }
    }
    Ok(())
}
