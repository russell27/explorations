// VARIABLES

// Naming convention :
// The name of a variable can be composed of letters, digits, and the underscore character.
// It must begin with either a letter or an underscore.
// Upper and lowercase letters are distinct because Rust is case-sensitive.
//
// SYNTAX :
// let variable_name = value;            // no type specified
// let variable_name:dataType = value;   //type specified


// By default, variables are immutable − read only in Rust. In other words, the variable's value cannot be changed once a value is bound to a variable name.
//
// It throws an error : "re-assignment of immutable variable" . The error message indicates the cause of the error – you cannot assign values twice to immutable variable fees. This is one of the many ways Rust allows programmers to write code and takes advantage of the safety and easy concurrency.
//
// Mutable : Variables are immutable by default. Prefix the variable name with mut keyword to make it mutable. The value of a mutable variable can be changed.


fn main() {
   let student_name = "Tanmayee";                // immutable string
   println!("student name :{}",student_name);
   let mut marks = 95;                           // mutable string
   println!("Marks obtained : {}",marks);
   let mut marks = 100;                         // changing the value of the string
   println!("Marks obtained after recorrection : {}",marks);
   println!();
   // Shadowing of variables : Rust allows programmers to declare variables with the same name. In such a case, the new variable overrides the previous variable.");
  shadowing();
}


fn shadowing() {
   let cost = 65.00;
   let cost = 45.50 ;
   // reads first cost
   println!("The value of cost is :{}",cost);
   println!();
   // The below code declares two variables by the name my_name. The first declaration is assigned a string value, whereas the second declaration is assigned an integer. The len function returns the total number of characters in a string value.
   let my_name = "";
   println!("name is : {}" , my_name);
   let my_name = my_name.len();
   println!("name changed to integer : {}",my_name);
}
