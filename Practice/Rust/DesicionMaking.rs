// DESICION MAKING

// if - An if statement consists of a Boolean expression followed by one or more statements.
//
fn if_condition() { 
    let num = 10;
    if num == 10 {
        println!("The number is 10");
    }
}

// if else - An if statement can be followed by an optional else statement, which executes when the Boolean expression is false.

fn if_else_condition() {
    let num = 10;
    if num % 2 == 0 {
        println!("Even number");
    }
    else {
        println!("Odd number");
    }
}

// You can use one if or else if statement inside another if or else if statement(s).
fn nested_if_condition() {
    let num = 0;
    if num > 0  { 
        println!("positive number");  }
    else if num < 0  {  
        println!("negative number");  }
    else   {
        println!("zero");  }
}

// The match statement checks if a current value is matching from a list of values, this is very much similar to the switch statement in C language.

fn match_condition() {
    let nick_name = "Tan";
    let name = match nick_name {
        "Tan" => "Tanmayee",
        "Van" => "Vennela",
        "Sid" => "Siddhanth",
        "Jyo" => "Jyothika",
        _ => "Unknown",
    };
    println!("I am {} alias {}",name ,nick_name);
}

fn is_leap_year() {
    let year = 1600;
    if year % 4 == 0 {
        if year % 100 == 0 {
            if year % 400 == 0 {
                println!("{} is a leap year.", year);
            }
            else {
                println!("{} is not a leap year.", year);
            }
        } else {
            println!("{} is a leap year.", year);
        }
    } else {
        println!("{} is not a leap year.", year);
    }
}

fn main() {
    if_condition();
    if_else_condition();
    nested_if_condition();
    match_condition();
    is_leap_year();
}

