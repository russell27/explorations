use std::io;
use std::io::*;
fn main(){
    let mut input = String::new();
    let mut sum = 0;
    println!("Enter number");
    io::stdin().read_line(&mut input).unwrap();
    let mut  n: i32 = input.trim().parse().unwrap();
    while n != 0{
        let r = n % 10;
        sum = sum + r;
        n = n / 10;
    }
    println!("sum of digits is {}",sum);
}
