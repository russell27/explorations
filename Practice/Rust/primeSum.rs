use std::io; 

fn is_prime(n: i32) -> bool {
    for a in 2..n {
        if n % a == 0 {
            return false; 
        }
    }
    true
}

fn prime_sum(n: i32) -> i32 {
    let mut primes = Vec::new();
    for i in 2..n+1 {
        if is_prime(i) {
            primes.push(i);
        }
    }
    let prime_sum = primes.iter().fold(0,|a, &b| a + b);
    return prime_sum
}
fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let n: i32 = input.trim().parse().unwrap();
    println!("{}",prime_sum(n));
}
