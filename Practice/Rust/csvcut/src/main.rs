extern crate csv;
extern crate argparse;
extern crate prettytable;
use prettytable::{Table, Row};
use std::io;
use argparse::{ArgumentParser,StoreTrue, List};
use std::error::Error;
use std::collections::HashMap;

fn main() -> Result<(),Box<dyn Error>> {
   let mut csv_table = Table::new();
   //let mut csv_row = Row::new();
   let mut headers = false;
   let _headers_of_columns = false;
   let mut no_of_columns:Vec<usize> = Vec::new();
   let mut header_names:Vec<String> = Vec::new();
   //let mut allRows = Vec::new();
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut headers)
            .add_option(&["-n", "--headers"],StoreTrue,"names");
        ap.refer(&mut no_of_columns)
            .add_option(&["-c", "--columns"],List,"columns");
        ap.parse_args_or_exit();
    }

    if headers{
        let mut reader = csv::Reader::from_reader(io::stdin());
        let headers = reader.headers()?;
        for i in 0..headers.len(){
             println!("{}.{}",i + 1,&headers[i]);
        }   }

    if no_of_columns.len() > 0{
        let mut col_header_names:Vec<String> = Vec::new();
        let _wtr = csv::Writer::from_writer(io::stdout());
        let mut csv_read = csv::ReaderBuilder::new()
        .from_reader(io::stdin());

        let headers_of_columns = csv_read.headers()?;
        for i in headers_of_columns.iter(){
            header_names.push(i.to_string());
    }
        for i in no_of_columns.iter(){
            col_header_names.push(headers_of_columns[*i].to_string());
        }
        //wtr.write_record(&col_header_names)?;
        let row1 = Row::from(col_header_names);
        csv_table.add_row(row1);
        for result in csv_read.deserialize() {
            let record: HashMap<String,String> = result?;
            let mut rows:Vec<String> = Vec::new();
            for i in no_of_columns.iter(){
                let row = &record[&header_names[*i].to_string()];
                rows.push(row.to_string());

            }
            //println!("{:?}",rows);
            //wtr.write_record(&rows)?;
            let row2 = Row::from(rows);
            csv_table.add_row(row2);
           // allRows.push_all(rows);
            }

        //wtr.flush()?;
        csv_table.printstd();
   
        }
 
    Ok(())
}
