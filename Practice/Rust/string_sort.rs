use std::iter::Iterator;
use std::iter::FromIterator;

fn main() {
    let mut wordy = String::new();
    println!("Enter the string :");
    std::io::stdin().read_line(&mut wordy).unwrap();
    let s_slice: &str = &wordy[..];
    let mut chars: Vec<char> = s_slice.chars().collect();
    chars.sort_by(|a, b| a.cmp(b));
    let s = String::from_iter(chars);
    println!("{}", s);
}
