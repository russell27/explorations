use std::io;

fn multiples(x:i32) {
    let mut sum = 0;
    for i in 1..x {
        if i % 3 == 0 || i % 5 == 0 {
            sum = sum + i;
        }
    }
    println!("The sum is {}",sum);
}
fn main() {
    let mut a_str = String::new();
    io::stdin().read_line(&mut a_str).expect("read error");
    let mut a_iter = a_str.split_whitespace();
    let n = a_iter.next().unwrap().parse::<i32>().expect("parse error");
    multiples(n)
}
