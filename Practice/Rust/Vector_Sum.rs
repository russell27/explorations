
fn main() {
    let cin = std::io::stdin();
    let mut s = String::new();
    println!("Enter the vector elements:");
    cin.read_line(&mut s).unwrap();
    let values = s.split_whitespace().map(|x| x.parse::<u32>()).collect::<Result<Vec<u32>, _>>().unwrap();
    let sum = values.iter().fold(0,|a, &b| a + b); 
    println!("{}",sum);
}
