extern crate alphanumeric_sort;
extern crate dmsort;

pub fn str_sort() {
    let mut names = ["-1","4","3"];
	alphanumeric_sort::sort_str_slice(&mut names);
	println!("{:?}",names);
}

pub fn num_sort() {
    let mut numbers =  [0, 1, 6, 7, 2, 3, 4, 5];
	dmsort::sort(&mut numbers);
	println!("{:?}",numbers);
}
