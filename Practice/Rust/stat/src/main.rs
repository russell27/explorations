mod methods;
extern crate argparse;
extern crate csv;
use argparse::{ArgumentParser, Store};
use methods::math_operations;
use methods::stat_operations;
use std::collections::HashMap;
use std::error::Error;
use std::io;
use std::result::Result;
type Record = HashMap<String, String>;
fn main() -> Result<(), Box<dyn Error>> {
    let mut column = 1;
    let mut name = String::new();
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut column)
            .add_option(&["-c", "--column"], Store, "column");
        ap.refer(&mut name)
            .add_option(&["-n", "--name"], Store, "name");
        ap.parse_args_or_exit();
    }
    let mut csv_read = csv::Reader::from_reader(io::stdin());
    let mut keys = Vec::new();
    {
        let _header = csv_read.headers()?;
    }
    let mut data = Vec::new();
    for i in csv_read.deserialize() {
        let record: Record = i?;
        data.push(record);
    }
    let k = csv_read.headers()?;
    for rec in k.iter() {
        keys.push(rec);
    }
    for i in 0..keys.len() {
        if keys[i] == name {
            column = i + 1
        }
    }
    let mut cols = Vec::new();
    for key in keys.iter() {
        let mut values = Vec::new();
        for i in data.iter() {
            for (k, val) in i {
                if k == key {
                    values.push(val);
                }
            }
        }
        cols.push(values);
    }
    let required_col = cols[column - 1].clone();
    let _value = required_col[0];
    if _value.parse::<u32>().is_ok() || _value.parse::<f64>().is_ok() {
        math_operations(&required_col);
        stat_operations(&required_col);
    }
    Ok(())
}
