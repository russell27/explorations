extern crate quickersort;
pub fn math_operations(column: &Vec<&String>) {
    let mut typecasted_col = Vec::new();
    for i in column.iter() {
        typecasted_col.push(i.parse::<f64>().unwrap_or(0.00)); // typecasting each value as f64 and appending it
    }
    let greater = typecasted_col
        .iter()
        .max_by(|a, b| a.partial_cmp(b).unwrap());
    let smaller = typecasted_col
        .iter()
        .min_by(|a, b| a.partial_cmp(b).unwrap());
    match greater {
        Some(x) => println!("Smallest value: {:?}", x),
        None => println!("0.00"),
    }
    match smaller {
        Some(x) => println!("Largest value: {:?}", x),
        None => println!("0.00"),
    }
    let sum: f64 = typecasted_col.iter().sum();
    println!("Sum: {:?}", sum);
}

fn get_frequency(vec: &Vec<f64>, element: f64) -> usize {
    vec.iter().filter(|&n| *n == element).count() // returns the frequency of an element in the vector
}

pub fn stat_operations(column: &Vec<&String>) {
    let mut typecasted_col = Vec::new();

    for i in column.iter() {
        typecasted_col.push(i.parse::<f64>().unwrap_or(0.00)); // typecasting each value as f64 and appending it
    }

    let sum: f64 = typecasted_col.iter().sum(); // calculating sum

    let mean: f64 = sum as f64 / column.len() as f64; // calculating mean
    println!("Mean: {:?}", mean);

    quickersort::sort_floats(&mut typecasted_col[..]); // sorting the vector of floats using quickersort
    let mid = typecasted_col.len() / 2;
    if typecasted_col.len() % 2 == 0 {
        let median = (typecasted_col[mid - 1] + typecasted_col[mid]) / 2.0;
        println!("Median: {:?}", median);
    } else {
        println!("Median: {:?}", typecasted_col[mid]);
    }

    let mut uniques = typecasted_col.clone();

    uniques.sort_by(|a, b| a.partial_cmp(b).expect("NaN in vector"));
    uniques.dedup();

    let mut frequencies: Vec<(&f64, usize)> = vec![]; // creating a vector to store the element and its frequency as a tuple
    for i in &uniques {
        frequencies.push((&i, get_frequency(&typecasted_col, *i)))
    }

    frequencies.sort_by_key(|k| k.1); // sorting the vector of tuples based on the frequency
    frequencies.reverse();

    let max_frequency = frequencies[0].1; // the frequency of most-repeated values
    
    let modes = frequencies.iter().filter(|n| n.1 == max_frequency);
    
    println!("Mode: "); // printing the mode values
    for mode in modes {
        print!("{}  ", mode.0);
    }
    println!(" ");
    println!("Uniques: "); // printing the unique values
    for unique in &uniques {
        print!("{} ", unique);
    }
    
}
