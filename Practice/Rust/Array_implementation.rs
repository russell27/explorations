// ACESSING THE ARRAYS


// NOTE : variables cannot be used to define the size of an array.

fn main() {
    let arr:[i32;4] = [5,10,15,20];   // declaring an array of integer type and size 4
    let neg_arr = [-1,-2,-3,-4];
    let fruits:[&str;4] = ["apple" ,"mango" , "cherry" ,"orange"];  // declaring an array of string type and size 4
    println!("integer array {:?}" ,arr);
    println!("fruits array {:?}" , fruits);
    println!("negative array {:?}" , neg_arr);
    accessing_array();
    accesing_with_iter();
    mutable_array();
}

fn accessing_array()  {
    let fruits:[&str;4] = ["apple" ,"mango" , "cherry" ,"orange"];
    for i in  0..4 {
        println!("{}",fruits[i]);
    }
}
fn accesing_with_iter() {
    let fruits:[&str;4] = ["apple" ,"mango" , "cherry" ,"orange"];
    for i in fruits.iter() {
        println!("{}",i);
    }
}

// The mut keyword can be used to declare a mutable array.
fn mutable_array() {
   let mut arr:[i32;4] = [10,20,30,40];
   arr[1] = 0;
   println!("{:?}",arr);
}



