extern crate argparse;
extern crate csv;
use std::error::Error;
use std::io;
use std::collections::HashMap;
use argparse::{ArgumentParser, Store, StoreTrue,List};

fn main()   -> Result<(),Box<dyn Error>>{

    let mut pattern = false;
    let mut verbose = false;
    let mut header_names:Vec<String> = Vec::new();
    let mut columns:Vec<usize> = Vec::new();
    let mut col_header_names:Vec<String> = Vec::new();
    let mut name = "World".to_string();
    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut pattern)
            .add_option(&["-i", "--verbose"], StoreTrue,
            "Be verbose");
        ap.refer(&mut columns)
            .add_option(&["-s", "--columns"],List,"columns");
        ap.refer(&mut verbose).add_option(&["-m","--names"],StoreTrue,"Be names");
       ap.refer(&mut name)
            .add_option(&["-n","--name"], Store,
            "Name for the greeting");
        ap.parse_args_or_exit();
    }

    

    if pattern {

    let mut rdr =  csv::Reader::from_reader(io::stdin());
    let mut wtr = csv::Writer::from_writer(io::stdout());
    
    wtr.write_record(rdr.headers()?)?;
    for result in rdr.records() {
        let record = result?;
        if !!!record.iter().any(|field| field.contains(&name)) {
            wtr.write_record(&record)?;
        }
    }

    wtr.flush()?;
    }

    if verbose {
        let mut rdr =  csv::Reader::from_reader(io::stdin());
        let mut wtr = csv::Writer::from_writer(io::stdout());
        wtr.write_record(rdr.headers()?)?;
        for result in rdr.records() {
            let record = result?;
            if record.iter().any(|field| field.contains(&name)) {
                wtr.write_record(&record)?;
            }
        }
        wtr.flush()?;
    }

     if columns.len() >= 0{
        let mut wtr = csv::Writer::from_writer(io::stdout());
        let mut csv_read = csv::ReaderBuilder::new()
        .from_reader(io::stdin());
    
        let headers = csv_read.headers()?;
        for i in headers.iter(){
            header_names.push(i.to_string());
    }
       for i in columns.iter(){
            col_header_names.push(headers[*i].to_string());
        }

        wtr.write_record(&col_header_names)?;
        let _values:Vec<String> = Vec::new();
        for result in csv_read.deserialize() {
            let record: HashMap<String,String> = result?;
            for val in record.values() {
                if val.contains(&name) {
                     let mut rows:Vec<String> = Vec::new();
                    for i in columns.iter() {
                        let row = &record[&header_names[*i].to_string()];
                        rows.push(row.to_string());
                    }
                      wtr.write_record(&rows)?;
                }
            }
                
    }
    wtr.flush()?;
     }
    Ok(())
}

