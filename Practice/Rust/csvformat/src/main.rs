extern crate csv;
extern crate argparse;
use std::io;
use argparse::{ArgumentParser,StoreTrue,List,Store};
use std::error::Error;
use std::collections::HashMap;


fn main() -> Result<(),Box<dyn Error>> {
   let mut symbol = " ".to_string();
   let mut newline = " ".to_string();
   let mut tabspace = false;
   let mut no_of_columns:Vec<usize> = Vec::new();
   let mut header_names:Vec<String> = Vec::new();

    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut symbol)
            .add_option(&["-D", "--symbol"],Store,"symbol");
        ap.refer(&mut tabspace)
            .add_option(&["-T", "--tabspace"],StoreTrue,"tabspace");
        ap.refer(&mut newline)
            .add_option(&["-M", "--newline"],Store,"newline");
        ap.parse_args_or_exit();
    }

    if (symbol != " "){
        let mut wtr = csv::Writer::from_writer(vec![]);
        //let mut wtr1 = csv::Writer::from_writer(io::stdout());
        let mut rdr = csv::Reader::from_reader(io::stdin());
        let headers = rdr.headers()?;
        wtr.write_record(headers);
        for result in rdr.records() {
            let record = result.expect("a CSV record");
            wtr.write_record(&record)?;

    }
        let data = String::from_utf8(wtr.into_inner()?)?;
        let sentence = data.replace(",", &symbol);
        println!("{}",&sentence);
    }
    if tabspace {
        let mut wtr = csv::Writer::from_writer(vec![]);
        let mut rdr = csv::Reader::from_reader(io::stdin());
        let headers = rdr.headers()?;
        wtr.write_record(headers)?;
        for result in rdr.records() {
            let record = result.expect("a CSV record");
            wtr.write_record(&record)?;

    }
        let data = String::from_utf8(wtr.into_inner()?)?;
        let sentence = data.replace(",", "  ");
        println!("{}",sentence);
    }
    if (newline != " "){
        let mut total = " ".to_string();
        let mut wtr = csv::Writer::from_writer(vec![]);
        let mut rdr = csv::Reader::from_reader(io::stdin());
        let headers = rdr.headers()?;
        wtr.write_record(headers);
        for result in rdr.records() {
            let record = result.expect("a CSV record");
            wtr.write_record(&record)?;


    }
        let data = String::from_utf8(wtr.into_inner()?)?;
        let sentence = data.replace("\n",&newline);
        println!("{}",&sentence);
    }

  
    Ok(())
}