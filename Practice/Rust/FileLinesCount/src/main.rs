use linecount::count_lines;
use std::fs::File;

fn main(){

    let lines: usize = count_lines(File::open("data.txt").unwrap()).unwrap();
    println!("\n count of lines is {}\n", lines);   
    
}
