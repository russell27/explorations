// STRING OBJECT METHODS

fn T0_STRING() {   // convert a string literal to object type using the to_string() function
    let name = "Tanmayee";
    println!("{}",name.to_string());
}

fn REPLACE() {  // The replace() function takes two parameters − the first parameter is a string pattern to search for and the second parameter is the new value to be replaced.
    let name = "I Tanmayee";
    println!("{}",name.replace("I","Imadabathuni"))
}

// The as_str() function extracts a string slice containing the entire string.

fn PUSH() {  //  The push() function appends the given char to the end of this String.
    let mut name = "Tanmayee ".to_string();
    name.push('I');
    println!("{}" , name);
}

fn PUSH_STR() {  //  The push_str() function appends a given string slice onto the end of a String.
    let mut name = "Tanmayee ".to_string();
    name.push_str("Imadabathuni");
    println!("{}" , name);
}

fn LEN() {  //  The len() function returns the total number of characters in a string (including spaces).
    let mut name = "Tanmayee ".to_string();
    println!("length is {}" ,name.len());
}

fn TRIM() { // The trim() function removes leading and trailing spaces in a string. NOTE that this function will not remove the inline spaces.
    let mut name = "   Tanmayee    ".to_string();
    println!("length of {} before triming is : {} ",name , name.len());
    println!("length of {} after triming is : {} ",name , name.trim().len());
}

fn SPLIT_WHITESPACES() {  //  The split_whitespace() splits the input string into different strings. It returns an iterator .
    let msg = "Tanmayee is a good girl .";
    for i in msg.split_whitespace(){
        println!("{}",i);
    }
}

fn CHARS() {  // Individual characters in a string can be accessed using the chars method.
    let mut name = "Tanmayee ".to_string();
    for i in name.chars() {
        println!("{}",i);
    }
}

fn concat() { // The + operator internally uses an add method. The result of string concatenation is a new string object. The first parameter is self – the string object itself and the second parameter is a reference of the second string object.

   let firstname = "Imadabathuni".to_string();
   let lastname = "Tanamyee".to_string();

   let fullname = firstname + &lastname; // lastname reference is passed
   println!("{}",fullname);
}

fn FORMAT_MACRO() { // Another way to add to String objects together is using a macro function called format.

    let firstname = "Imadabathuni".to_string();
    let lastname = "Tanamyee".to_string();
    let fullname = format!("{} {}",firstname , lastname);
    println!("{}" , fullname);
}


fn typeCasting() {
   let number = 2020;
   let number_as_string = number.to_string();

   // convert number to string
   println!("{}",number_as_string);
   println!("{}",number_as_string=="2020");
}


//  The split() string method returns an iterator over substrings of a string slice, separated by characters matched by a pattern. The limitation of the split() method is that the result cannot be stored for later use. The collect method can be used to store the result returned by split() as a vector.

fn SPLIT() {
    let details = "Imadabathuni,Tanmayee,BVRITH";

   for i in details.split(","){
      println!("{}",i);
   }

   //store in a Vector
   println!("\n");
   let i:Vec<&str>= details.split(",").collect();
   println!("firstName is {}",i[0]);
   println!("lastname is {}",i[1]);
   println!("company is {}",i[2]);
}

fn main() {
    T0_STRING();
    REPLACE();
    PUSH();
    PUSH_STR();
    LEN();
    TRIM();
    SPLIT_WHITESPACES();
    CHARS();
    concat();
    FORMAT_MACRO();
    typeCasting();
    SPLIT();
}
