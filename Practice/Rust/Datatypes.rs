// DATATYPES

fn main() {
    primitive_data_types();
    println!();
    integer_datatypes();
    println!();
    float_datatypes();
    println!();
    number_seperator();
    println!();
    boolean_datatypes();
    println!();
    character_datatypes();
}


fn primitive_data_types() {
   let name = "I . Tanmayee";        // string type
   let age = 18;                     // integer type
   let salary = 500.45;              // float type
   let is_good_girl = true;          // boolean type
   let icon = '♥';                   //unicode character type

   println!("My name is:{}",name);
   println!("I am {} yeras old.",age);
   println!("I earn {} rupees per day.",salary);
   println!("Am i a good girl ? {}",is_good_girl);
   println!("I {} chocolates...!",icon);
}

fn integer_datatypes() {
   let result = 10;    // i32 by default
   let age:u32 = 20;
   let sum:i32 = 5-15;
   let mark:isize = 10;
   let count:usize = 30;
   println!("result value is {}",result);
   println!("sum is {} and age is {}",sum,age);
   println!("mark is {} and count is {}",mark,count);
}



fn float_datatypes() {
   let result = 10.00;        //f64 by default
   let interest:f32 = 8.35;
   let cost:f64 = 15000.600;  //double precision

   println!("result value is {}",result);
   println!("interest is {}",interest);
   println!("cost is {}",cost);
}

// Automatic type casting is not allowed in Rust. 
// Example : let interest:f32 = 8;   // integer assigned to float variable
// The compiler throws a mismatched types error as given below.


fn number_seperator() {
   // For easy readability of large numbers, we can use a visual separator _ underscore to separate digits.
   
    let float_with_separator = 11_000.555_001;
   println!("float value {}",float_with_separator);

   let int_with_separator = 50_000;
   println!("int value {}",int_with_separator);
}

fn boolean_datatypes() {
   let isfun:bool = true;
   println!("Is Rust Programming Fun ? {}",isfun);
   let boring:bool = false;
   println!("Is Rust programming boring ? {}",boring);
}


fn character_datatypes() {
   let special_character = '@'; //default
   let alphabet:char = 'A';
   let emoji:char = '😁';

   println!("special character is {}",special_character);
   println!("alphabet is {}",alphabet);
   println!("emoji is {}",emoji);
}

