extern crate prettytable;
use prettytable::{Table};
extern crate csv;
use std::error::Error;
use std::io;
fn main() -> Result<(),Box<dyn Error>>{
    let mut csv_read = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(io::stdin());
    let csv_table = Table::from_csv(&mut csv_read);
    csv_table.printstd();
    Ok(())

}
