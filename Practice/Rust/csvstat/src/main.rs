extern crate csv;
use csv::Error;
use std::collections::HashSet;
//use std::cmp::PartialEq<{integer}>;


fn main() -> Result<(), Error> {
    let csv =  "name,number,place,thing
                shivani,23,hyd,fan
                guna,56,delhi,mouse";
    let mut csv_read = csv::Reader::from_reader(csv.as_bytes());

    let mut sorted_data = Vec::new();
    for record in csv_read.records() {
        sorted_data.push(record.unwrap()[1].trim().parse::<i32>().unwrap());
    }
    let mut zero_count = 0;
    for i in &sorted_data{
        if *i == 0{
            zero_count = zero_count + 1;
        }
    }
    if zero_count > 0{
        println!("Null : True");
    }
    else{
        println!("Null : False");
    }
    
    let mut sum = 0;
      for x in &sorted_data {
          sum = sum + *x;
      }
    println!("Sum     : {}",sum);

    let length = sorted_data.len();
    println!("length  : {}",length);
   
    //let mut unique: HashSet:<i32> = sorted_data.into_iter().collect().len();
    
    let mean = sum as i32 / length as i32;
    println!("Mean    : {}",mean);
    
   // let median = sorted_data.iter().median();
    //println!("Median : {}",median);

    let min_value = sorted_data.iter().min();
    let max_value = sorted_data.iter().max();

    match min_value {
        Some(min)  => println!( "Min value  : {}", min ),
         None      => println!( "Vector is empty" ),
}
    match max_value {
    Some(max) => println!( "Max value : {}", max ),
    None      => println!( "Vector is empty" ),
}
   // println!("{:?}",unique);

    Ok(())
}
    
