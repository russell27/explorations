use std::io;
use std::io::*;
fn main(){
    let mut input = String::new();
    let mut  rev = 0;
    println!("Enter number");
    io::stdin().read_line(&mut input).unwrap();
    let mut n: i32 = input.trim().parse().unwrap();
    while n != 0{
        let r = n % 10;
        rev = (rev * 10) + r;
        n = n / 10;
    }
    println!("Reverse of a number is {}",rev);
}
