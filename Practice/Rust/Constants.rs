//  CONSTANTS

// Constants represent values that cannot be changed. If you declare a constant then there is no way its value changes. The keyword for using constants is const. Constants must be explicitly typed.
// SYNTAX  :  const VARIABLE_NAME:dataType = value;


fn main() {
   const totat_marks:i32 = 100;    // Declare a integer constant
   const e:f32 = 2.73;           //Declare a float constant

   println!("The question paper is made for {} marks",totat_marks);  //Display value of the constant
   println!("'e' value is {}",e);            //Display value of the constant
}

// Unlike variables, constants cannot be shadowed. If variables in the above program are replaced with constants, the compiler will throw an error : "    already defined "
