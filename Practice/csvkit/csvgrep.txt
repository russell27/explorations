-->head -20 artworks.csv | csvcut -c Date,DateAcquired | csvlook
|----------+---------------|
|  Date    | DateAcquired  |
|----------+---------------|
|  1896    | 1996-04-09    |
|  1987    | 1995-01-17    |
|  1903    | 1997-01-15    |
|  1980    | 1995-01-17    |
|  1903    | 1997-01-15    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|  1976-77 | 1995-01-17    |
|----------+---------------|
While the first 20 values in DateAcquired look fine, the Date column has some values that won’t play nicely with most data visualization tools like 1976-77. We can easily deal with this by just selecting the first year in the range (e.g. 1976 from the range 1976-77). Before we do that, let’s figure out how many lines match this pattern.

We can use the csvgrep tool to extract all values in a column (or columns) that match a regular expression. We specify the columns we want csvgrep to match on using the -c flag. We specify the regular expression we want csvgrep to use using the -regex flag. The regex ^([0-9]*-[0-9]*) matches pairs of numeric values that are separated by a hyphen (-).

Since we’re searching for instances of the pattern on the Date column, we write the following:



-->csvgrep --c Date --regex "^([0-9]*-[0-9]*)"
Let’s modify and run the pipeline we’ve built to incorporate csvgrep:

-->head -10 artworks.csv | csvcut -c Date | csvgrep --c Date --regex "^([0-9]*-[0-9]*)" | csvlook
|-----------|
|  Date     |
|-----------|
|  1976-77  |
|  1976-77  |
|  1976-77  |
|  1976-77  |
|-----------|

Even though ‘-m’, ‘-r’, and ‘-f’ are listed as “optional” arguments, you must specify one of them.




