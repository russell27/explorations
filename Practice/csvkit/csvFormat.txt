                                                CSV FORMAT


Csvformat is basically used to get the output in te required form.


syntax :
    csvformat [OPTIONS] [File_name]

    
Options :

    1. -T   --->    Used to get a tab space between each column.
    
    2. -U {0, 1, 2, 3}   --->    Used to determine the quoting style in the output[0 is specified for quotting the minimal occurance,
                                 1 is for quoting each and every cell, 2 is for quoting all the non-numeric cells
                                 & 3 is for not quoting any cell].
    
    3. -D \[delimiter]   --->    Used to determine the delimiting character[By default it is comma].
    
    4. -M \[terminator]   --->    Used to determine the terminator.
    
    5. -h   --->    Used to display help message
    
    
Examples :

    1. csvformat -D \$ [file_name]  --  displays output with "$" as delimiter instead of ","
    
    2. csvformat -M \* [file_name]  --  displays output with "*" as terminator(i.e., at the end of each row)