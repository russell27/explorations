
<<<<<<< HEAD
						  **OVERVIEW ON CSVSTACK**
=======
				                      **CSV toolkit Analysis**

						      **Overview on csvstack**
>>>>>>> 0bdcb9838951bc5ed1b3a5ca6eef9843684fe389



Before I get into the tool analysis let us quickly go through the general overview for better understanding.


CSV toolkit:

It is a toolkkit written in python which work on csvfiles in an efficent manner and easier than the unix text processing tools for the cvs kind of files, the commands or the utilities are broadly divided in to three catogories:

* Input
* Processing/power
* Output

Let us go through the csvstack command.


csvstack:

Before we get into the code let us know what does the csvstack does.
All the example files used are placed in the examples folder.


csvstack is one of the power utilities.

Frequently large datasets are distributed in many small files. At some point you will probably want to merge those files for bulk analysis. csvstack allows you to “stack” the rows from CSV files with the same columns (and identical column names).

Let us assume we have two regions Nebraska and Kansa and their datasets are ne_1033_data.csv and ks_1033_data.csv  
They both have same columns.

Something like this:

~$ csvcut -n ne_1033_data.csv 
  1: state
  2: county
  3: fips
  4: nsn
  5: item_name
  6: quantity
  7: ui
  8: acquisition_cost
  9: total_cost
 10: ship_date
 11: federal_supply_category
 12: federal_supply_category_name
 13: federal_supply_class
 14: federal_supply_class_name



~$ csvcut -n ks_1033_data.csv 
  1: state
  2: county
  3: fips
  4: nsn
  5: item_name
  6: quantity
  7: ui
  8: acquisition_cost
  9: total_cost
 10: ship_date
 11: federal_supply_category
 12: federal_supply_category_name
 13: federal_supply_class
 14: federal_supply_class_name


Now to stack these two files so that i get all details related to both the regions in one file.

~$ csvstack ne_1033_data.csv ks_1033_data.csv > region.csv
~$ csvcut -n region.csv 
  1: state
  2: county
  3: fips
  4: nsn
  5: item_name
  6: quantity
  7: ui
  8: acquisition_cost
  9: total_cost
 10: ship_date
 11: federal_supply_category
 12: federal_supply_category_name
 13: federal_supply_class
 14: federal_supply_class_name

If I go for stats of two columns in ne_1033_data.csv file i.e county and state

~$ csvstat -c county,state ne_1033_data.csv 
  2. "county"

	Type of data:          Text
	Contains null values:  False
	Unique values:         35
	Longest value:         10 characters
	Most common values:    DOUGLAS (760x)
	                       DAKOTA (42x)
	                       CASS (37x)
	                       HALL (23x)
	                       LANCASTER (18x)

  1. "state"

	Type of data:          Text
	Contains null values:  False
	Unique values:         1
	Longest value:         2 characters
	Most common values:    NE (1036x)

Row count: 1036


If I go for stats of two columns in ks_1033_data.csv file i.e county and state

~$ csvstat -c county,state ks_1033_data.csv 
  2. "county"

	Type of data:          Text
	Contains null values:  False
	Unique values:         73
	Longest value:         12 characters
	Most common values:    WYANDOTTE (123x)
	                       FINNEY (103x)
	                       SHAWNEE (77x)
	                       SEDGWICK (63x)
	                       SALINE (59x)

  1. "state"

	Type of data:          Text
	Contains null values:  False
	Unique values:         1
	Longest value:         2 characters
	Most common values:    KS (1575x)

Row count: 1575



If I go for stats of two columns in region.csv file i.e county and state

~$ csvstat -c county,state region.csv 
  2. "county"

	Type of data:          Text
	Contains null values:  False
	Unique values:         102
	Longest value:         12 characters
	Most common values:    DOUGLAS (802x)
	                       WYANDOTTE (123x)
	                       FINNEY (103x)
	                       SHAWNEE (77x)
	                       SEDGWICK (63x)

  1. "state"

	Type of data:          Text
	Contains null values:  False
	Unique values:         2
	Longest value:         2 characters
	Most common values:    KS (1575x)
	                       NE (1036x)

Row count: 2611


We can see that the both files are stacked.




Now to get familiar with the datasets I tried out some examples.Here is one using the text processing tools

~$ csvcut -c county ks_1033_data.csv|sort|uniq -c|sort -nbr|head -n 10
    123 WYANDOTTE
    103 FINNEY
     77 SHAWNEE
     63 SEDGWICK
     59 SALINE
     56 MIAMI
     49 GEARY
     43 CRAWFORD
     42 DOUGLAS
     41 RENO

~$ csvcut -c county ne_1033_data.csv|sort|uniq -c|sort -nbr|head -n 10
    760 DOUGLAS
     42 DAKOTA
     37 CASS
     23 HALL
     18 LANCASTER
     12 DAWSON
     11 MADISON
     11 BURT
      8 HOLT
      8 COLFAX

~$ csvcut -c county region.csv|sort|uniq -c|sort -nbr|head -n 10
    802 DOUGLAS
    123 WYANDOTTE
    103 FINNEY
     77 SHAWNEE
     63 SEDGWICK
     59 SALINE
     56 MIAMI
     49 GEARY
     43 CRAWFORD
     42 DAKOTA



csvstack has a option called -g which is used for the grouping purpose, I am still working on it.




		    **TO GET THE CODE ANALYSIS REFER THE DOC IN NOTES NAMED csvstack in csvkit directory**

